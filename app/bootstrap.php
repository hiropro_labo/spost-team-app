<?php

// Load in the Autoloader
require COREPATH.'classes'.DIRECTORY_SEPARATOR.'autoloader.php';
class_alias('Fuel\\Core\\Autoloader', 'Autoloader');

// Bootstrap the framework DO NOT edit this
require COREPATH.'bootstrap.php';


Autoloader::add_classes(array(
	// Add classes you want to override here
	// Example: 'View' => APPPATH.'classes/view.php',
	'Basecontroller'               => APPPATH.'classes/lib/basecontroller.php',
	'Basecontroller_Rest'          => APPPATH.'classes/lib/basecontroller_rest.php',
	'Model_Crud_Shard'             => SUPPORTPATH.'models/model_crud_shard.php',
    'Model_Crud_Master'            => SUPPORTPATH.'models/model_crud_master.php',
    'Model_Crud_Shard_Image'       => SUPPORTPATH.'models/model_crud_shard_image.php',
    'Model_Crud_Shard_Token'       => SUPPORTPATH.'models/model_crud_shard_token.php',
    'Model_Crud_Shard_Payment'     => SUPPORTPATH.'models/model_crud_shard_payment.php',
	'Model_Readonly'               => SUPPORTPATH.'models/model_readonly.php',

	// Supported Classes
    'Team'                         => SUPPORTPATH.'team.php',
    'Support\\Team\\App'           => SUPPORTPATH.'team/app.php',
    'Support\\Team\\Contents'      => SUPPORTPATH.'team/contents.php',
    'Support\\Team\\Sponsor'       => SUPPORTPATH.'team/sponsor.php',
    'Sponsor'                      => SUPPORTPATH.'sponsor.php',
    'Notification'                 => SUPPORTPATH.'notification.php',
    'Switcher'                     => SUPPORTPATH.'switcher.php',

    // Supported Classes: Contents
    'Support\\Contents\\Base'            => SUPPORTPATH.'contents/base.php',
    'Support\\Contents\\Image'           => SUPPORTPATH.'contents/image.php',
    'Support\\Contents\\News'            => SUPPORTPATH.'contents/news.php',
    'Support\\Contents\\Gallery'         => SUPPORTPATH.'contents/gallery.php',
    'Support\\Contents\\Icon'             => SUPPORTPATH.'contents/icon.php',
    'Support\\Contents\\Good\\History'   => SUPPORTPATH.'contents/good/history.php',
    'Support\\Contents\\Shop\\Profile'   => SUPPORTPATH.'contents/shop/profile.php',
    'Support\\Contents\\Menu\\Category'  => SUPPORTPATH.'contents/menu/category.php',
    'Support\\Contents\\Menu\\Item'      => SUPPORTPATH.'contents/menu/item.php',
    'Support\\Contents\\Device\\Token'   => SUPPORTPATH.'contents/device/token.php',
    'Support\\Contents\\Team\\Member'    => SUPPORTPATH.'contents/team/member.php',
    'Support\\Contents\\Cheer\\Member'   => SUPPORTPATH.'contents/cheer/member.php',
    'Support\\Contents\\Game\\Schedule'  => SUPPORTPATH.'contents/game/schedule.php',
    'Support\\Contents\\Game\\Result'    => SUPPORTPATH.'contents/game/result.php',

    // Supported Classes: Contents for Sponsor
    'Support\\Contents\\Status\\Sponsor'          => SUPPORTPATH.'contents/status/sponsor.php',
    'Support\\Contents\\Status\\Sponsor\\Top'     => SUPPORTPATH.'contents/status/sponsor/top.php',
    'Support\\Contents\\Status\\Sponsor\\News'    => SUPPORTPATH.'contents/status/sponsor/news.php',
    'Support\\Contents\\Status\\Sponsor\\Store'   => SUPPORTPATH.'contents/status/sponsor/store.php',
    'Support\\Contents\\Status\\Sponsor\\Coupon'  => SUPPORTPATH.'contents/status/sponsor/coupon.php',
    'Support\\Contents\\Status\\Sponsor\\Menu'    => SUPPORTPATH.'contents/status/sponsor/menu.php',

    // Supported Classes: Contents for Team
    'Support\\Contents\\Status\\Team'             => SUPPORTPATH.'contents/status/team.php',
    'Support\\Contents\\Status\\Team\\Top'        => SUPPORTPATH.'contents/status/team/top.php',
    'Support\\Contents\\Status\\Team\\News'       => SUPPORTPATH.'contents/status/team/news.php',
    'Support\\Contents\\Status\\Team\\Store'      => SUPPORTPATH.'contents/status/team/store.php',
    'Support\\Contents\\Status\\Team\\Coupon'     => SUPPORTPATH.'contents/status/team/coupon.php',
    'Support\\Contents\\Status\\Team\\Menu'       => SUPPORTPATH.'contents/status/team/menu.php',

    // Supported Classes: Sponsor
    'Support\\Sponsor\\App'                     => SUPPORTPATH.'sponsor/app.php',
    'Support\\Sponsor\\App\\Inspect'            => SUPPORTPATH.'sponsor/app/inspect.php',
    'Support\\Sponsor\\App\\Contents'           => SUPPORTPATH.'sponsor/app/contents.php',
    'Support\\Sponsor\\App\\Contents\\Status'   => SUPPORTPATH.'sponsor/app/contents/status.php',
    'Support\\Sponsor\\App\\Contents\\Store'    => SUPPORTPATH.'sponsor/app/contents/store.php',
    'Support\\Sponsor\\App\\Contents\\Switcher' => SUPPORTPATH.'sponsor/app/contents/switcher.php',

    // Supported Classes: Team
    'Support\\Team\\App'                     => SUPPORTPATH.'team/app.php',
    'Support\\Team\\App\\Inspect'            => SUPPORTPATH.'team/app/inspect.php',
    'Support\\Team\\App\\Contents'           => SUPPORTPATH.'team/app/contents.php',
    'Support\\Team\\App\\Contents\\Status'   => SUPPORTPATH.'team/app/contents/status.php',
    'Support\\Team\\App\\Contents\\Store'    => SUPPORTPATH.'team/app/contents/store.php',
    'Support\\Team\\App\\Contents\\Switcher' => SUPPORTPATH.'team/app/contents/switcher.php',

    'Support\\Api\\Base'                 => SUPPORTPATH.'api/base.php',
    'Support\\Api\\Team\\Cheer\\Member'  => SUPPORTPATH.'api/team/cheer/member.php',
    'Support\\Api\\Team\\Global_Info'    => SUPPORTPATH.'api/team/global_info.php',
    'Support\\Api\\Team\\Member'         => SUPPORTPATH.'api/team/member.php',
    'Support\\Api\\Team\\Schedule'       => SUPPORTPATH.'api/team/schedule.php',
    'Support\\Api\\Team\\Gallery'        => SUPPORTPATH.'api/team/gallery.php',
    'Support\\Api\\Team\\Result'         => SUPPORTPATH.'api/team/result.php',
    'Support\\Api\\Team\\Setting'        => SUPPORTPATH.'api/team/setting.php',
    'Support\\Api\\Team\\Sponsor'        => SUPPORTPATH.'api/team/sponsor.php',
    'Support\\Api\\Team\\Switcher'       => SUPPORTPATH.'api/team/switcher.php',
    'Support\\Api\\Team\\Token'          => SUPPORTPATH.'api/team/token.php',
    'Support\\Api\\Team\\Top'            => SUPPORTPATH.'api/team/top.php',
    'Support\\Api\\Team\\News'           => SUPPORTPATH.'api/team/news.php',
    'Support\\Api\\Team\\News\\Token'    => SUPPORTPATH.'api/team/news/token.php',
));

// Register the autoloader
Autoloader::register();

/**
 * Your environment.  Can be set to any of the following:
 *
 * Fuel::DEVELOPMENT
 * Fuel::TEST
 * Fuel::STAGING
 * Fuel::PRODUCTION
 */
Fuel::$env = (isset($_SERVER['FUEL_ENV']) ? $_SERVER['FUEL_ENV'] : Fuel::STAGING);

// Initialize the framework with the config file.
Fuel::init('config.php');
