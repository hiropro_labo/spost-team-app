{include "common/header.tpl"}

<div id="content"><!-- 固定ヘッダー+余白分 -->

<!-- トップ画像 -->
<div class="flexslider">
    <ul class="slides">
    {foreach from=$list_recommend item=item}
    {if $item->type == '0'}
    <li><img src="http://timg.spost.jp/app/top/{$TEAM->id()}/{$item->file_name}" alt="TOP画像1" /></li>
    {else}
    <li>
        <div class="iframeWrap" onclick="jump('{$item->movie_url}')">
            <iframe width="640" height="425" style="z-index:1;" src="//www.youtube.com/embed/{$item->movie_url}?autohide=1&rel=0&showinfo=1&controls=0&fs=0&modestbranding=1" frameborder="0" allowfullscreen></iframe>
            <img style="position:absolute;width:100%;height:100%;z-index:1000;top:0;left:0;" src="/assets/img/common/img_clear.png" />
        </div>
    </li>
    {/if}
    {/foreach}
</ul>
</div>
<div class="clear"></div>

<!-- サブメニュー -->
<div class="sub_menu">
    <ul>
        <li><a href="http://www.fivearrows.jp/partner/#support"><img src="/assets/img/app/{$TEAM->id()}/common/button_middle_sponsor.png" alt="パートナー募集!!" class="sub_menu_img" /></a></li>
        <li><a href="http://www.fivearrows.jp/news/club/entry-1067.html"><img src="/assets/img/app/{$TEAM->id()}/common/button_middle_member.png" alt="会員募集中!!" class="sub_menu_img" /></a></li>
        <li><a href="http://www.fivearrows.jp/academy/arrows_academy/"><img src="/assets/img/app/{$TEAM->id()}/common/button_middle_academy.png" alt="受講生募集中" class="sub_menu_img" /></a></li>
    </ul>
</div><!-- (/sub_menu) -->

<div class="clear"></div>

<!-- サブコンテンツ -->
<div class="game">
    <div id="game">
        <!--    <a id="prev" href="#"><img src="/assets/img/arrow/arrow_top.png" alt="<<前へ" class="top_arrow_prev" /></a>||
        -->
        <div class="top_arrow">
            <a id="next" href="#"><img src="/assets/img/arrow/arrow_top.png" alt="次へ>>" class="top_arrow_next" /></a>
        </div>
        <div class="game_box">
            <div id="left">
            {foreach from=$list item=item}
                {if $item->result}
                {* ----- 未開催：スケジュール ----- *}
                <div class="scl">
                     <p class="place">{$item->place}</p>
                    <div class="hi">
                         <p>{$item->opened_at|date_format:"%m/%d"}</p>
                    </div>
                    <div class="jikan_box">
                         <p>{$item->opened_at|date_format:"%H:%M"}<br /><span class="week">{$item->opened_at|date_format:"%a"|replace:"月":"Mon"|replace:"火":"Tue"|replace:"水":"Wed"|replace:"木":"Thu"|replace:"金":"Fri"|replace:"土":"Sat"|replace:"日":"Sun"}</span></p>
                    </div>
                    <p class="game_team"><img src="/assets/img/top/icon_1.png" alt="{$TEAM->region()}" class="team_left" /><img src="/assets/img/top/icon_2.png" alt="{$item->enemy->region()}" class="team_right" /></p>
                </div>
                {else}
                {* ----- 終了：試合結果 ---------- *}
                <div class="scl2">
                     <p class="game_date fc_w">{$item->opened_at|date_format:"%m月%d日(%a)"|replace:"Sun":"日"|replace:"Mon":"月"|replace:"Tue":"火"|replace:"Wed":"水"|replace:"Thu":"木"|replace:"Fri":"金"|replace:"Sat":"土"}</p>
                    <p class="fc_y game_end">試合終了</p>
                    <p class="score"><span class="fc_y">{$item->result->score_own}</span>-<span class="space">{$item->result->score_enemy}</span></p>
                    <p class="game_team"><img src="/assets/img/top/icon_1.png" alt="{$TEAM->region()}" class="team_left" /><img src="/assets/img/top/icon_2.png" alt="{$item->enemy->region()}" class="team_right" /></p>
                </div>
                {/if}
            {/foreach}
            </div>
        </div>
<div class="clear"></div>
    </div><!-- (game) -->
</div>

<!-- NEWS -->
<div class="sub_contents">
    <p><img src="/assets/img/app/{$TEAM->id()}/common/titlebar_news.png" alt="news" class="sub_contents_title" /></p>
    <!--<p><img src="../images/top/titlebar_marc.png" alt="チームマーク" class="title_background" /></p>-->
</div><!-- (/sub_contents) -->

<div class="clear"></div>

<div>
    <ul class="sub_contents_main">
      {if $list_news}
      {foreach from=$list_news item=item}
        <li><a href="/news/detail/{$TEAM->id()}/{$item->id}"><span class="scm_text">{$item->title}</span><span><img src="/assets/img/app/{$TEAM->id()}/common/arrow_yellow.png" alt=">" class="yellow_arrow" /></span></a></li>
      {/foreach}
      {else}
      <li><p class="news_no_entry">現在ニュースはありません</p></li>
      {/if}
  </ul>
  <div class="clear"></div>
</div>


<div class="clear"></div>

<!-- SPONSOR -->
<div class="sub_contents">
    <p><img src="/assets/img/app/{$TEAM->id()}/common/titlebar_sponsor.png" alt="sponsor" class="sub_contents_title" /></p>
</div><!-- (/sub_contents) -->


<!----Official sponsor---->
<div class="ribon">
    <p><img src="/assets/img/app/{$TEAM->id()}/common/ribbon_sponsor.png" alt="SPONSOR" class="ribon_img" /></p>
</div><!-- (/ribon) -->


<div id="sponser">
<div class="sponser_s">
    <p><a href="/sponsor/{$TEAM->id()}"><img src="/assets/img/app/{$TEAM->id()}/common/button_aplist.png" alt="スポンサーアプリ一覧" class="sponser_btn" /></a></p>
</div><!-- (/sponser_s) -->

<div class="clear"></div>

<div class="sponser">
    <img src="/assets/img/app/{$TEAM->id()}/common/sponsor_list.png" alt="SPONSOR_LIST" />
</div><!-- /Official -->

</div>

<div class="clear"></div>


<!-- フッター -->
<div id="foot">
    <div class="footer">
        <ul>
          {if $social}
            {if !empty($social->facebook)}
            <li><a href="{$social->facebook}"><img src="/assets/img/app/{$TEAM->id()}/common/icon_facebook.png" width="36px" height="36px" alt="Facebook" class="icon_li" /></a></li>
            {/if}
            {if !empty($social->twitter)}
            <li><a href="{$social->twitter}"><img src="/assets/img/app/{$TEAM->id()}/common/icon_twitter.png" width="36px" height="36px" alt="Twitter" class="icon_li" /></a></li>
            {/if}
            {if !empty($social->youtube)}
            <li><a href="{$social->youtube}"><img src="/assets/img/app/{$TEAM->id()}/common/icon_youtube.png" width="36px" height="36px" alt="youtube" class="icon_li" /></a></li>
            {/if}
          {/if}
        </ul>
    </div>
        <div class="clear"></div>
<div class="copy">{$TEAM->contents()->store()->copyright()}</div>
</div><!-- (/foot) -->
</div><!-- (/#content) -->

{literal}
<script type="text/javascript">
function jump(param){
	window.location.href = 'http://youtu.be/' + param;
}
</script>
{/literal}

{include "common/footer.tpl"}