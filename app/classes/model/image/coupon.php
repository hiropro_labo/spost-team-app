<?php
/**
 * Model COUPON
 *
 * @author     Kouji Itahana
 */

class Model_Image_Coupon extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_COUPON';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'coupon';

}