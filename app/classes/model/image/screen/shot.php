<?php
/**
 * Model スクリーンショット
 *
 * @author     Kouji Itahana
 */

class Model_Image_Screen_Shot extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_SCREEN_SHOT';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'screen_shot1';

    /**
     * カラム[link]がある場合はtrue
     * @var bolean
     */
    protected static $_column_link = false;

}