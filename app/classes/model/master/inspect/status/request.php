<?php
use Fuel\Core\Model_Crud;

/**
 * Model アプリ審査申請ステータス
 *
 * @author     Kouji Itahana
 */

class Model_Master_Inspect_Status_Request extends Model_Crud_Master
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'M_INSPECT_STATUS_REQUEST';
}