<?php
/**
 * Model タブ情報
 *
 * @author     Kouji Itahana
 */

class Model_Tab_Item extends Model_Crud_Shard
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'TAB_ITEM';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * @var array datetime columns
     */
    protected static $_datetime_columns = array(
        'updated_at', 'created_at'
    );
}
