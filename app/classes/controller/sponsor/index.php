<?php
/**
 * SPONSOR
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Sponsor_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'sponsor/';
    /**
     * action 前処理
     */
    protected function before_controller()
    {
        //static::$_client_id = $this->setup_shop();
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

        $list = self::$_team->sponsor()->list_all();

        $list_sponsor = array();
        foreach ($list as $model)
        {
            $sponsor_id = $model->id;
            $sponsor = new \Sponsor($sponsor_id);
            if ( ! $sponsor->app()->is_opened_android()) continue;

            $holder = array();
            $holder['sponsor_id']       = $sponsor->id();
            $holder['app_name']         = $sponsor->app()->app_name();
            $holder['file_name']        = $sponsor->contents()->icon()->model()->file_name;
            $holder['sponsor_name']     = $sponsor->contents()->shop_profile()->model()->shop_name;
            $holder['app_url_ios']      = $sponsor->contents()->store_url()->model()->apple;
            $holder['app_url_android']  = $sponsor->contents()->store_url()->model()->google;
            array_push($list_sponsor, $holder);
        }
        \Team::setup_shard();

        $view->set('list', $list_sponsor);
	    return $view;
	}
}
