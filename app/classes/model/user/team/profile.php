<?php
use Fuel\Core\Model_Crud;

/**
 * Model チームプロフィール
 *
 * @author     Kouji Itahana
 */

class Model_User_Team_Profile extends Model_Crud
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'USER_TEAM_PROFILE';

    protected static $_mysql_timestamp = true;

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';
}