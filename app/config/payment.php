<?php
/**
 * JPayment 決済関連設定ファイル
 */

return array(
    /**
     * JPayment決済データ送信先URL(※相対パス)
     */
    //'send_url' => 'https://credit.j-payment.co.jp/gateway/payform.aspx',
    'send_url' => 'http://admin.i-post.local/payment/apply/confirm',

    /**
     * 店舗ID
     */
    'aid' => '105505',

    /**
     * FORMパラメータ: カード決済画面表示
     */
    'pt' => '1',
);
