<?php
/**
 * Model 対戦スケジュールテーブル
 *
 * @author     Kouji Itahana
 */

class Model_Game_Result extends Model_Crud_Shard
{
    const JUDGE_WIN  = 1;
    const JUDGE_LOSE = 2;
    const JUDGE_DRAW = 3;

    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'GAME_RESULT';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * スコアから勝敗結果を取得
     *
     * @return int
     */
    public function judge()
    {
        if (intval($this->score_own) > intval($this->score_enemy))  return self::JUDGE_WIN;
        if (intval($this->score_own) < intval($this->score_enemy))  return self::JUDGE_LOSE;
        if (intval($this->score_own) == intval($this->score_enemy)) return self::JUDGE_DRAW;
    }
}