<?php
/**
 * TICKET
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Ticket_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'ticket/';
    /**
     * action 前処理
     */
    protected function before_controller()
    {
        //static::$_client_id = $this->setup_shop();
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $team_id = self::$_team->id();
	    $view = View::forge(self::VIEW_FILE_PREFIX."index{$team_id}.tpl");
	    return $view;
	}
}
