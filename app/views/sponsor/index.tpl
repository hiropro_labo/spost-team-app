{include "common/header_member_detail.tpl"}

<div id="content"><!-- 固定ヘッダー+余白分 -->

<div id="sponsor">

  {if empty($list)}
  <p class="no_data">登録アプリは0件です。</p>
  {else}
  <div class="item">
  {foreach from=$list item=item}
      <div class="box">
      <a href="{$item['app_url_android']}"><img src="http://img.spost.jp/app/store/{$item['sponsor_id']}/{$item['file_name']}" width="58" height="58" /></a>
      <div class="app_info">
        <p class="app_name">アプリ名<span><a href="#" style="color:#F60">{$item['app_name']}</a></span></p>
        <p>{$item['sponsor_name']}</p>
      </div>
    </div>
  {/foreach}
  </div>
  {/if}

</div>

{include "common/footer.tpl"}