<?php
/**
 * NEWS
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_News_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'news/';
    /**
     * action 前処理
     */
    protected function before_controller()
    {
        //static::$_client_id = $this->setup_shop();
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

	    $list = self::$_team->contents()->news_info()->list_sent_news_web();

	    $view->set('list', $list);
	    return $view;
	}

	 /**
     * いいね履歴判定
     *
     * @param string $news_id
     * @param string $token device-token
     * @return boolean true:有り/false:無し
     */
    private function is_exists_good($news_id, $token)
    {
        return self::$_team->contents()->good_history()->is_exists_good($news_id, $token);
    }

	/**
	 * Detail
	 *
	 * @access public
	 * @return Response
	 */
	public function action_detail()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'detail.tpl');

		$token = 'bf7aa7d1705934970546feb9f4b5b6eaaee32a7b03b815e4b015be9e485e5f19';
	    
	    $id = $this->param('id');
	    if (empty($id))
	    {
	        Response::redirect(404);
	    }

	    //$token = Input::post('token');
	    if (empty($token))
	    {
	        Response::redirect(404);
	    }

	    $news = self::$_team->contents()->news_info()->model_by_id($id);
		$news->good = $this->is_exists_good($news->id, $token) ? 1 : 0;

	    $view->set('news', $news);
	    $view->set('token', $token);
	    return $view;
	}

    /**
     * action method: detail
     *
     * @return View
     *//*
    public function detail($id, $token)
    {
        $view = static::view('news/detail.tpl');

        $model = static::sponsor()->contents()->news_info()->model_by_id($id);
        $model->good = $this->is_exists_good($model->id, $token) ? 1 : 0;
        
        $view->set('model', $model);
        $view->set('token', $token);
        return $view;
    }*/
}
