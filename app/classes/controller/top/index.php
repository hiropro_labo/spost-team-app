<?php
/**
 * TOP
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Top_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'top/';
    /**
     * action 前処理
     */
    protected function before_controller()
    {
        //static::$_client_id = $this->setup_shop();
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

	    $list_recomment = self::$_team->contents()->recommend()->model_list();
	    $list_news = self::$_team->contents()->news_info()->list_news_top(5);
	    $social = self::$_team->contents()->social_url()->model();
	    $list = self::$_team->contents()->schedule()->model_list();

	    foreach ($list as $schedule)
	    {
	        $schedule->enemy = new \Team($schedule->enemy);
	        $result = self::$_team->contents()->result()->model_by_schedule_id($schedule->id);
	        if ( ! is_null($result))
	        {
	            $schedule->status = 1;
	            $schedule->result = $result;
	        }
	    }

        $view->set('list',   $list);
	    $view->set('list_recommend', $list_recomment);
	    $view->set('list_news',      $list_news);
	    $view->set('social', $social);
	    return $view;
	}

	public function action_carousel()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'carousel.tpl');

	    $list = self::$_team->contents()->schedule()->model_list();
	    foreach ($list as $schedule)
	    {
	        $schedule->enemy = new \Team($schedule->enemy);
	        $result = self::$_team->contents()->result()->model_by_schedule_id($schedule->id);
	        if ( ! is_null($result))
	        {
	            $schedule->status = 1;
	            $schedule->result = $result;
	        }
	    }

        $view->set('list',   $list);
	    return $view;
	}
}
