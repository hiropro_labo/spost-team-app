<?php
/**
 * Model トークン情報
 *
 * @author     Yoshitaka Kitagawa
 */

class Model_Device_Token extends Model_Crud_Shard_Token
{
    /**
     * デバイス種別: iOS
     * @var int
     */
    const DEVICE_TYPE_IOS = 1;

    /**
     * デバイス種別: Android
     * @var int
     */
    const DEVICE_TYPE_ANDROID = 2;

    /**
     * 通知設定: 通知なし
     * @var int
     */
    const ALLOW_FLG_OFF = 0;

    /**
     * 通知設定: 通知あり
     * @var int
     */
    const ALLOW_FLG_ON  = 1;

    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'DEVICE_TOKEN';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * @var array datetime columns
     */
    protected static $_datetime_columns = array(
        'updated_at', 'created_at'
    );
}