{include "common/header_member_detail.tpl"}


<div id="content"><!-- 固定ヘッダー+余白分 -->

    <img src="http://timg.spost.jp/app/result/top/{$TEAM->id()}/{$image->file_name}" style="position:fixed; width:100%">

<div id="result" style="padding-top:27%; width:100%">

    <!--item-->
    <div class="item">

    {foreach from=$list item=item}
      <div class="box">
        <div class="schedule">
        {if $item->home_and_away == 0}
          <img src="/assets/img/app/{$TEAM->id()}/schedule/icon_away.png" alt="away" />
        {else}
          <img src="/assets/img/app/{$TEAM->id()}/schedule/icon_away.png" alt="away" />
        {/if}
          <p>{$item->opened_at|date_format:"%Y年%m月%d日(%a)"|replace:"Sun":"日"|replace:"Mon":"月"|replace:"Tue":"火"|replace:"Wed":"水"|replace:"Thu":"木"|replace:"Fri":"金"|replace:"Sat":"土"}</p>
        </div>
        <!--wrap-->
        {if $item->score_own == $item->score_enemy}
        <div class="wrap">
          <div class="draw">
            <div class="score_left">
              {if $item->score_own > 99}
              <img src="/assets/img/app/{$TEAM->id()}/result/score/w{$item->score_own|substr:0:1}.png"><img src="/assets/img/app/{$TEAM->id()}/result/score/y{$item->score_own|substr:1:1}.png"><img src="/assets/img/app/{$TEAM->id()}/result/score/y{$item->score_own|substr:2:1}.png">
              {else}
              <img src="/assets/img/app/{$TEAM->id()}/result/score/w{$item->score_own|substr:0:1}.png"><img src="/assets/img/app/{$TEAM->id()}/result/score/y{$item->score_own|substr:1:1}.png">
              {/if}
              <p>{$item->region_own}</p>
            </div>
            <div class="score_right">
              {if $item->score_enemy > 99}
              <img src="/assets/img/app/{$TEAM->id()}/result/score/w{$item->score_enemy|substr:0:1}.png"><img src="/assets/img/app/{$TEAM->id()}/result/score/w{$item->score_enemy|substr:1:1}.png"><img src="/assets/img/app/{$TEAM->id()}/result/score/w{$item->score_enemy|substr:2:1}.png">
              {else}
              <img src="/assets/img/app/{$TEAM->id()}/result/score/w{$item->score_enemy|substr:0:1}.png"><img src="/assets/img/app/{$TEAM->id()}/result/score/w{$item->score_enemy|substr:1:1}.png">
              {/if}
              <p>{$item->region_enemy}</p>
            </div>
          </div>
        </div>
        {else}
        <div class="wrap">
          <div class="{if $item->judge == 2}win_left{else}win_right{/if}">
            <div class="score_left">
              {if $item->score_own > 99}
              <img src="/assets/img/app/{$TEAM->id()}/result/score/y{$item->score_own|substr:0:1}.png"><img src="/assets/img/app/{$TEAM->id()}/result/score/y{$item->score_own|substr:1:1}.png"><img src="/assets/img/app/{$TEAM->id()}/result/score/y{$item->score_own|substr:2:1}.png">
              {else}
              <img src="/assets/img/app/{$TEAM->id()}/result/score/y{$item->score_own|substr:0:1}.png"><img src="/assets/img/app/{$TEAM->id()}/result/score/y{$item->score_own|substr:1:1}.png">
              {/if}
              <p>{$item->region_own}</p>
            </div>
            <div class="score_right">
              {if $item->score_enemy > 99}
              <img src="/assets/img/app/{$TEAM->id()}/result/score/w{$item->score_enemy|substr:0:1}.png"><img src="/assets/img/app/{$TEAM->id()}/result/score/w{$item->score_enemy|substr:1:1}.png"><img src="/assets/img/app/{$TEAM->id()}/result/score/w{$item->score_enemy|substr:2:1}.png">
              {else}
              <img src="/assets/img/app/{$TEAM->id()}/result/score/w{$item->score_enemy|substr:0:1}.png"><img src="/assets/img/app/{$TEAM->id()}/result/score/w{$item->score_enemy|substr:1:1}.png">
              {/if}
              <p>{$item->region_enemy}</p>
            </div>
          </div>
        </div>
        {/if}
        <!--/wrap-->
      </div>

    {/foreach}

    </div>
    <!--/item-->
</div>


{include "common/footer.tpl"}