{include "common/header.tpl"}

<body style="min-height:0px; height:92px;">
<div id="">
<div class="game" style="padding-bottom: 0; padding-top: 6px;">
    <div id="game">
        <!--    <a id="prev" href="#"><img src="/assets/img/arrow/arrow_top.png" alt="<<前へ" class="top_arrow_prev" /></a>||
        -->
        <div class="top_arrow">
            <a id="next" href="#"><img src="/assets/img/arrow/arrow_top.png" alt="次へ>>" class="top_arrow_next" /></a>
        </div>
        <div class="game_box">
            <div id="left">
            {foreach from=$list item=item}
                {if $item->result}
                {* ----- 未開催：スケジュール ----- *}
                <div class="scl">
                     <p class="place">{$item->place}</p>
                    <div class="hi">
                         <p>{$item->opened_at|date_format:"%m/%d"}</p>
                    </div>
                    <div class="jikan_box">
                         <p>{$item->opened_at|date_format:"%H:%M"}<br /><span class="week">{$item->opened_at|date_format:"%a"|replace:"月":"Mon"|replace:"火":"Tue"|replace:"水":"Wed"|replace:"木":"Thu"|replace:"金":"Fri"|replace:"土":"Sat"|replace:"日":"Sun"}</span></p>
                    </div>
                    <p class="game_team"><img src="/assets/img/top/icon_1.png" alt="{$TEAM->region()}" class="team_left" /><img src="/assets/img/top/icon_2.png" alt="{$item->enemy->region()}" class="team_right" /></p>
                </div>
                {else}
                {* ----- 終了：試合結果 ---------- *}
                <div class="scl2">
                     <p class="game_date fc_w">{$item->opened_at|date_format:"%m月%d日(%a)"|replace:"Sun":"日"|replace:"Mon":"月"|replace:"Tue":"火"|replace:"Wed":"水"|replace:"Thu":"木"|replace:"Fri":"金"|replace:"Sat":"土"}</p>
                    <p class="fc_y game_end">試合終了</p>
                    <p class="score"><span class="fc_y">{$item->result->score_own}</span>-<span class="space">{$item->result->score_enemy}</span></p>
                    <p class="game_team"><img src="/assets/img/top/icon_1.png" alt="{$TEAM->region()}" class="team_left" /><img src="/assets/img/top/icon_2.png" alt="{$item->enemy->region()}" class="team_right" /></p>
                </div>
                {/if}
            {/foreach}
            </div>
        </div>
<div class="clear"></div>
    </div><!-- (game) -->
</div>
</div>
</body>
</html>
