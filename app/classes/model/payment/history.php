<?php
use Fuel\Core\Model_Crud;

/**
 * Model 決済履歴
 *
 * @author     Kouji Itahana
 */

class Model_Payment_History extends Model_Crud_Shard_Payment
{

    /**
     * @var int クレジットカード決済
     */
    const PAYMENT_TYPE_CREDIT = 1;

    /**
     * @var int 口座振込み決済
     */
    const PAYMENT_TYPE_BANK  = 6;

    /**
     * @var string ベーシックコース
     */
    const SALE_ITEM_BASIC = 'basic_1month_auto';

    /**
     * @var string ベーシックコース (1ヶ月間) キャンペーン価格
     */
    const SALE_ITEM_BASIC_CP = 'cp_1month_auto';

    /**
     * @var string ベーシックコース　1ヶ月間の無料期間付き
     */
    const SALE_ITEM_BASIC_FREE = 'cp_1month_free_auto';

    /**
     * @var string ベーシックコース　1ヶ月間の無料期間付き＋キャンペーン価格
     */
    const SALE_ITEM_BASIC_CP_FREE = 'cp_1month_free_plus_auto';

    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'PAYMENT_HISTORY';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';
}