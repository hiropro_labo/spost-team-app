<?php
/**
 * TEAM
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Team_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'team/';
    /**
     * action 前処理
     */
    protected function before_controller()
    {
        //static::$_client_id = $this->setup_shop();
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

	    $image = self::$_team->contents()->team_top()->model();
	    $list  = self::$_team->contents()->team_member()->list_active_members();

	    $view->set('image', $image);
	    $view->set('list',  $list);
	    return $view;
	}

    /**
	 * DETAIL
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_detail()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'detail.tpl');

	    $id = $this->param('id');
	    if (empty($id))
	    {
	        Response::redirect(404);
	    }
	    $member = self::$_team->contents()->team_member()->get_member($id);

	    $view->set('member', $member);
	    return $view;
	}
}
