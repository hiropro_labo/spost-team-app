<?php
use Fuel\Core\Model_Crud;

/**
 * Model ゴールドパートナーユーザ
 *
 * @author     Kouji Itahana
 */

class Model_View_Client extends Model_Readonly
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'VIEW_CLIENT';

	/**
	 * Finds all records in the table.  Optionally limited and offset.
	 *
	 * @param   int     $limit     Number of records to return
	 * @param   int     $offset    What record to start at
	 * @return  null|object        Null if not found or an array of Model object
	 */
	public static function find_all($limit = null, $offset = 0, $alive = true)
	{
	    $config = array('limit' => $limit, 'offset' => $offset);
	    $alive ? $config['where'] = array(array('del', '=', 0)) : null;
		return static::find($config);
	}
}