<?php
/**
 * SETTING
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Setting_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'setting/';
    /**
     * action 前処理
     */
    protected function before_controller()
    {
        //static::$_client_id = $this->setup_shop();
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

	    $token = $this->param('token');

	    $notif = null;
	    $model = self::$_team->contents()->device_token()->model_by_token($token);
	    if ( ! is_null($model))
	    {
	        $notif = $model->allow_flg;
	    }

	    $view->set('token', $token);
	    $view->set('notif', $notif);
	    return $view;
	}
}
