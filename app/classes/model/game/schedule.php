<?php
/**
 * Model 対戦スケジュールテーブル
 *
 * @author     Kouji Itahana
 */

class Model_Game_Schedule extends Model_Crud_Shard
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'GAME_SCHEDULE';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * ゲームスケジュール画像パス
     *
     * @return string
     */
    public function match_image_path()
    {
        $conf = \Config::load('original_image', true);
        return $conf['img_common_path']."schedule/{$this->client_id}/team{$this->enemy}.png";
    }

    /**
     * 対戦相手のチーム名称取得
     *
     * @return string
     */
    public function enemy_name()
    {
        $model_enemy = \Model_Master_Team::find_one_by('id', $this->enemy);
        if (is_null($model_enemy)) return '';

        return $model_enemy->name;
    }

    /**
     * 開催日時を年月日時分にそれぞれ分解
     *
     * @param Model_Crud $model
     * @return Model_Crud
     */
    public function analyze_opened_at()
    {
        static::opened_at_year();
        static::opened_at_month();
        static::opened_at_day();
        static::opened_at_hour();
        static::opened_at_min();
    }

    /**
     * 試合開催日時
     *
     * @return \DateTime
     */
    public function opened_at()
    {
        return new \DateTime($this->opened_at);
    }

    /**
     * 試合開催日時：年
     *
     * @return string
     */
    public function opened_at_year()
    {
        $this->opened_at_year = static::opened_at()->format('Y');
        return $this->opened_at_year;
    }

    /**
     * 試合開催日時：月
     *
     * @return string
     */
    public function opened_at_month()
    {
        $this->opened_at_month = static::opened_at()->format('n');
        return $this->opened_at_month;
    }

    /**
     * 試合開催日時：日
     *
     * @return string
     */
    public function opened_at_day()
    {
        $this->opened_at_day = static::opened_at()->format('j');
        return $this->opened_at_day;
    }

    /**
     * 試合開催日時：時
     *
     * @return string
     */
    public function opened_at_hour()
    {
        $this->opened_at_hour = static::opened_at()->format('H');
        return $this->opened_at_hour;
    }

    /**
     * 試合開催日時：分
     *
     * @return string
     */
    public function opened_at_min()
    {
        $this->opened_at_min = static::opened_at()->format('i');
        return $this->opened_at_min;
    }

    /**
     * 試合結果レコード取得
     *
     * @return Model_Game_Result|NULL
     */
    public function result()
    {
        return \Model_Game_Result::find_one_by('schedule_id', $this->id);
    }

    /**
     * 試合結果の登録状況
     *
     * @return boolean true:登録済/false:未登録
     */
    public function is_registed_result()
    {
        $result = static::result();
        return is_null($result) ? false : true;
    }

    /**
     * 試合開始済み？
     *
     * @return boolean
     */
    public function is_opened()
    {
        $now = new DateTime();
        $opened_at = new DateTime($this->opened_at);
        return $opened_at < $now;
    }

}