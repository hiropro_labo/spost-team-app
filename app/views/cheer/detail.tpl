{include "common/header_member_detail.tpl"}

{include "common/sub_header.tpl"}

<div id="content"><!-- 固定ヘッダー+余白分 -->
<div id="team_member_detail">
  <img src="http://timg.spost.jp/app/cheer/member/{$TEAM->id()}/{$member->file_name}" class="w_100">

  <!--item-->
  <div class="item">
    <table>
      <tr><td class="td">誕生日</td><td>{$member->birth_month}月{$member->birth_date}日</td></tr>
      <tr><td class="td">趣味</td><td>{$member->hobby}</td></tr>
      <tr><td class="td">特技</td><td>{$member->ability}</td></tr>
      <tr><td class="td">出身地</td><td>{$member->home_town}</td></tr>
    </table>
  </div>
  <!--/item-->

</div>

{include "common/footer.tpl"}