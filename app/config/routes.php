<?php
return array(
    //---------------------------
    // Exception
    //---------------------------
	'_404_'   => 'exception/404',    // The main 404 route

    //---------------------------
    // top
    //---------------------------
    'top/carousel/(:team_id)' => 'top/index/carousel',
    'top/(:team_id)'          => 'top/index',

    //---------------------------
    // news
    //---------------------------
    'news/detail/(:team_id)/(:id)' => 'news/index/detail',
    'news/(:team_id)' => 'news/index',

    //---------------------------
    // result
    //---------------------------
    'result/(:team_id)' => 'result/index',

    //---------------------------
    // schedule
    //---------------------------
    'schedule/detail/(:team_id)/(:id)' => 'schedule/index/detail',
    'schedule/(:team_id)'              => 'schedule/index',

    //---------------------------
    // sponsor
    //---------------------------
    'sponsor/(:team_id)' => 'sponsor/index',

    //---------------------------
    // team
    //---------------------------
    'team/detail/(:team_id)/(:id)' => 'team/index/detail',
    'team/(:team_id)'              => 'team/index',

    //---------------------------
    // cheer
    //---------------------------
    'cheer/detail/(:team_id)/(:id)' => 'cheer/index/detail',
    'cheer/(:team_id)'              => 'cheer/index',

    //---------------------------
    // ticket
    //---------------------------
    'ticket/(:team_id)' => 'ticket/index',

    //---------------------------
    // gallery
    //---------------------------
    'gallery/detail/(:team_id)/(:id)' => 'gallery/index/detail',
    'gallery/(:team_id)'              => 'gallery/index',

    //---------------------------
    // sponsor
    //---------------------------
    'sponsor/(:team_id)' => 'sponsor/index',

    //---------------------------
    // setting
    //---------------------------
    'setting/notif'   => 'setting/notif/index',
    'setting/(:team_id)/(:token)' => 'setting/index',


);
