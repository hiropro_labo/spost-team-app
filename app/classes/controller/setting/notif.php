<?php
use Fuel\Core\Controller_Rest;

/**
 * Notification: 非同期更新
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Setting_Notif extends Basecontroller_Rest
{
    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $data = array(true, "");

	    try
	    {
	        DB::start_transaction();

	        $team_id = Input::json('team_id');
	        $team = new \Team($team_id);
	        $token = Input::json('token');
	        $flg   = Input::json('flg');

	        $this->check_token($team, $token);
            $this->check_flg($flg);

            $model = $team->contents()->device_token()->model_by_token($token);
	        if (is_null($model))
            {
                throw new Exception("更新対象データが見つかりませんでした");
            }

            if (intval($model->allow_flg) == intval($flg))
            {
                throw new Exception("更新されました");
            }

            $model->allow_flg = $flg;
            if ( ! $model->save(false))
            {
                throw new Exception("更新データの保存に失敗しました");
            }

	        DB::commit_transaction();
	    }
	    catch (\Exception $e)
	    {
            DB::rollback_transaction();
            Log::error("recruit info data update failed.   ".$e->getMessage());
            $data = array(false, $e->getMessage());
	    }

	    $json = json_encode($data);

	    $this->response->set_header('Content-Type', 'application/json; charset=utf-8');
	    return $this->response->body($json);
	}

	/**
	 * 入力値バリデーション: デバイストークン
	 *
	 * @param string $token
	 * @throws Exception
	 */
	private function check_token($team, $token)
	{
	    if (empty($token))
	    {
	        throw new Exception("不正なアクセスです");
	    }

	    if ( ! $team->contents()->device_token()->is_exsits_token($token))
	    {
	        throw new Exception("不正なアクセスです");
	    }
	}

	/**
	 * 入力値バリデーション: アプリからの通知受取り可否設定フラグ
	 *
	 * @param string|number $flg
	 * @throws Exception
	 */
	private function check_flg($flg)
	{
	    if (is_null($flg))
	    {
	        throw new Exception("不正なアクセスです");
	    }
	}

}
