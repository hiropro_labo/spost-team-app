<?php
/**
 * Model TOPページ：ショップ画像
 *
 * @author     Kouji Itahana
 */

class Model_Image_Shop extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_SHOP';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'shop';

    /**
     * カラム[link]がある場合はtrue
     * @var bolean
     */
    protected static $_column_link = false;

    /**
     * 検索結果がnullなら空のModelオブジェクトを返す
     *
     * @param string client-id
     * @return self
     */
    public static function get_image($client_id, $position=null)
    {
        $res = self::find_one_by('client_id', $client_id);
        if (is_null($res))
        {
            $res = self::forge();
            $res->client_id = $client_id;
            $res->position  = 1;
            $res->file_name = '';
        }
        return $res;
    }

}