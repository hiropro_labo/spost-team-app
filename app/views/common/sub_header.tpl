<div style="position:fixed; z-index:100; top:0px; left:0px; width:100%;">
    <div class="head">
        <div class="menu">
            <ul>
                <li><a href="/top/{$TEAM->id()}"><img src="/assets/img/app/{$TEAM->id()}/common/button_top.png" alt="TOP" class="menu_li" /></a></li>
                <li><a href="/schedule/{$TEAM->id()}"><img src="/assets/img/app/{$TEAM->id()}/common/button_schedule.png" alt="SCHEDULE" class="menu_li" /></a></li>
                <li><a href="/ticket/{$TEAM->id()}"><img src="/assets/img/app/{$TEAM->id()}/common/button_ticket.png" alt="TICKET" class="menu_li" /></a></li>
                <li><a href="/team/{$TEAM->id()}"><img src="/assets/img/app/{$TEAM->id()}/common/button_team.png" alt="TEAM" class="menu_li" /></a></li>
                <li><a href="/result/{$TEAM->id()}"><img src="/assets/img/app/{$TEAM->id()}/common/button_result.png" alt="RESULT" class="menu_li" /></a></li>
            </ul>
        </div><!-- (menu) -->
        <div class="clear"></div>
    </div><!-- (head) -->
</div><!-- (/position:fixed) -->
