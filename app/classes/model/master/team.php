<?php
use Fuel\Core\Model_Crud;

/**
 * Model チーム名称マスター
 *
 * @author     Kouji Itahana
 */

class Model_Master_Team extends Model_Crud
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'M_TEAM';

    /**
     * Select用Array生成
     *
     * @return array
     */
    public static function list_opt_array($my_id=null)
    {
        $opt = array('' => '----- 対戦相手を選択 -----');
        $list = self::find_all();
        foreach ($list as $entry)
        {
            if ( ! is_null($my_id) and $entry->id == $my_id)
            {
                continue;
            }
            $opt[$entry->id] = $entry->name;
        }
        return $opt;
    }
}