<?php
use Fuel\Core\Model_Crud;

/**
 * Model アプリステータス
 *
 * @author     Kouji Itahana
 */

class Model_Master_App_Status extends Model_Crud_Master
{
    /**
     * ステータス値：初期ステータス
     * @var int
     */
    const STATUS_INIT       = 100;
    /**
     * ステータス値：審査申請中
     * @var int
     */
    const STATUS_INSPECTED  = 200;
    /**
     * ステータス値：決済待機（※審査通過済）
     * @var int
     */
    const STATUS_PAYMENT_WAIT = 300;
    /**
     * ステータス値：決済完了（※弊社公開処理待ち）
     * @var int
     */
    const STATUS_PAYMENT_COMP = 400;
    /**
     * ステータス値：公開中
     * @var int
     */
    const STATUS_OK         = 500;
    /**
     * ステータス値：BAN（※アカウント停止）
     * @var int
     */
    const STATUS_BAN        = 600;
    /**
     * ステータス値：一時停止（※公開に問題がある等）
     * @var int
     */
    const STATUS_STOP       = 700;
    /**
     * ステータス値：有効期限切れ（※再決済の必要あり）
     * @var int
     */
    const STATUS_EXPIRED    = 800;
    /**
     * ステータス値：退会
     * @var int
     */
    const STATUS_LEAVE      = 900;

    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'M_APP_STATUS';

    /**
     * Select用Array生成
     *
     * @return array
     */
    public static function list_opt_array()
    {
        $opt = array('' => '----- ステータスを選択 -----');
        $list = self::find_all();
        foreach ($list as $entry)
        {
            $opt[$entry->id] = $entry->name.'('.$entry->type.')';
        }
        return $opt;
    }

    /**
     * ステータス：公開状態
     *
     * @return string
     */
    public static function status_type($status)
    {
        $rec = self::find_one_by('id', $status);
        if (is_null($rec)) return null;

        return $rec->type;
    }

    /**
     * ステータス名称取得
     *
     * @return string
     */
    public static function status_name($status)
    {
        $rec = self::find_one_by('id', $status);
        if (is_null($rec)) return null;

        return $rec->name.'('.$rec->type.')';
    }

    /**
     * ステータスコメント取得
     *
     * @return string
     */
    public static function status_comment($status)
    {
        $rec = self::find_one_by('id', $status);
        if (is_null($rec))
        {
            return null;
        }

        return $rec->comment;
    }
}
