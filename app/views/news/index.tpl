{include "common/header.tpl"}

  <img src="/assets/img/app/{$TEAM->id()}/news/header_news.png" style="position:fixed; width:100%">
<div id="news_list" style="padding-top:27%; width:100%">


<div id="content"><!-- 固定ヘッダー+余白分 --><!-- NEWS -->

    <ul class="sub_contents_main">
{foreach from=$list item=item}
        <li>
          <a href="/news/detail/{$TEAM->id()}/{$item->id}">
            <div class="date"><p class="month">{$item->updated_at|date_format:"%Y/%m/%d %a"|replace:"月":"MON"|replace:"火":"TUE"|replace:"水":"WED"|replace:"木":"THU"|replace:"金":"FRI"|replace:"土":"SAT"|replace:"日":"SUN"}.</p></div>
            <div class="scm_text_li">{$item->title}</div>
            <span><img src="/assets/img/app/{$TEAM->id()}/common/arrow_yellow.png" alt=">" class="yellow_arrow_li" /></span>
          </a>
        </li>
{/foreach}
    </ul>
	<div class="clear"></div>
</div><!-- (/sub_contents) -->


</div><!-- (/#content) -->


{include "common/footer.tpl"}