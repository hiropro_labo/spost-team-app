<?php
/**
 * Model ギャラリー画像
 *
 * @author     Kouji Itahana
 */

class Model_Image_Gallery extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_GALLERY';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'gallery';

    /**
     * ギャラリー画像リストをソートして返す
     * @param unknown $id
     * @return NULL|array
     */
    public static function list_image($id)
    {
        $list = self::find_by(array('client_id' => $id));
        if (is_null($list)) return;

        usort($list, function($a, $b){
            return $a['position'] > $b['position'];
        });
        return $list;
    }

}