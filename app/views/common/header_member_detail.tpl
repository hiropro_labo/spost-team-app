<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <link rel="stylesheet" type="text/css" href="/assets/css/team/{$TEAM->id()}/common.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/assets/css/team/{$TEAM->id()}/style2.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/assets/css/team/{$TEAM->id()}/garally.css" media="all" />
    <script type="text/javascript" src="/assets/js/jquery-1.9.0.js"></script>
    <script type="text/javascript" src="/assets/js/team/jquery.carouFredSel-6.2.1.js"></script>
    <script type="text/javascript" src="/assets/js/team/jquery.flexslider.js"></script>
    <script type="text/javascript" src="/assets/js/team/game.js"></script>
    <title>SPost</title>
</head>
<body>

