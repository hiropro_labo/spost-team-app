<?php
use Parser\View;

/**
 * Basecontroller
 *
 * コントローラ基底クラス
 *
 * @author     Kouji Itahana
 */

class Basecontroller extends Controller
{
	/**
	 * @var  Template Object
	 */
	protected $_view = null;

	/**
	 * @var Config Original
	 */
	protected static $_config = null;

    /**
     * @var string チームID
     */
    protected static $_team_id = null;

    /**
     * @var Team
     */
    protected static $_team = null;

	/**
	 * This method gets called before the action is called
	 */
	public function before()
	{
	    // 各種設定ファイルロード
	    $this->load_conf();

	    // メンテナンスモード
        $this->maintenance();

        // ユーティリティインスタンスセットアップ
        static::setup_utils();

	    // 各コントローラ独自の前処理
	    if (method_exists(static::$this, 'before_controller'))
	    {
	        $this->before_controller();
	    }
	}

	/**
	 * This method gets called after the action is called
	 */
	public function after($response)
	{
	    if ($response instanceof \View)
	    {
            foreach (self::$_config as $k => $v)
            {
                $response->set($k, $v);
            }

            foreach (\Config::load('original_image', true) as $k => $v)
            {
                $response->set($k, $v);
            }

            $response->set('TEAM', static::$_team);
	    }

	    $response = parent::after($response);
	    return $response;
	}

	/**
	 * 各種設定ファイルロード
	 */
	private function load_conf()
	{
	    self::$_config = \Config::load('original', true);
	    \Config::load('auth', true);
    }

    /**
     * メンテナンスモード
     *
     * メンテナンススイッチがONかつメンテ期間中なら503を返す
     */
    private function maintenance()
    {
        if (static::conf_get('maintenance_switch'))
        {
            $current = time();
            $m_start = strtotime(static::conf_get('maintenance_start_datetime'));
            $m_end   = strtotime(static::conf_get('maintenance_end_datetime'));
            if ($current >= $m_start and $current <= $m_end)
            {
                // APIなのでメンテナンスページを表示等はせず503を返す
                return Response::redirect('maintenance');
            }
        }
    }

	/**
	 * 設定ファイルから指定キーの値を取得
	 *
	 * @return    string
	 */
	protected static function conf_get($key, $group='original')
	{
	    return \Config::get($group.'.'.$key);
	}

	/**
	 * POSTのみ許可
	 */
	protected function check_only_post()
	{
	    if (Input::method() == 'POST') return;

	    Log::error(__METHOD__.'Invalid access to only POST page.');
	    return Response::redirect('exception/404');
	}

	/**
	 * Viewへjsをセット
	 *
	 * @param View
	 * @param string|array set-value
	 * @return View
	 */
	protected function set_view_js($view, $value)
	{
	    $js = $view->get('js', array());

	    if (is_array($value))
	    {
	        foreach ($value as $v)
	        {
	            array_push($js, $v);
	        }
	    }
	    else
	    {
	        array_push($js, $value);
	    }

	    $view->set('js', $js, false);
	    return $view;
	}

	/**
	 * Viewへcssをセット
	 *
	 * @param View
	 * @param string|array set-value
	 * @return View
	 */
	protected function set_view_css($view, $value)
	{
	    $css = $view->get('css', array());

	    if (is_array($value))
	    {
	        foreach ($value as $v)
	        {
	            array_push($css, $v);
	        }
	    }
	    else
	    {
	        array_push($css, $value);
	    }

	    $view->set('css', $css, false);
	    return $view;
	}

	/**
	 * チームID取得
	 *
	 * @return string team-id
	 */
	protected function team_id()
	{
	    static::$_team_id = $this->param('team_id');
        return static::$_team_id;
	}


	/**
	 * ユーティリティセットアップ
	 */
	protected function setup_utils($id=null)
	{
	    $team_id = static::team_id();
	    if (is_null($team_id))
	    {
	        $team_id = $id;
	    }

	    if ( ! empty($team_id))
	    {
            static::$_team = new \Team($team_id);
	    }
	}

}