<?php
/**
 * PUSH通知：配信処理
 *
 * ・Cronにて定期実行されることを前提としています
 * ・排他処理として各実行キュー(DBレコード)のステータスに応じて実行/スキップ
 *
 * @author     K.ITAHANA
 */

namespace Fuel\Tasks;

use Fuel\Core\Log;

class Notification
{
    /**
     * 定期実行されるメソッド：PUSH通知の配信処理
     */
    public static function run()
    {
        $conf = \Config::load('notification', true);
        $status_pre = \Model_Queue_Notification::QUEUE_STATUS_PRE;

        $queue = \Model_Queue_Notification::find_by('status', $status_pre, null, $conf['per_limit']);
        if (is_null($queue))
        {
            Log::info('Notification run success');
            return;
        }

        foreach ($queue as $q)
        {
            // キューステータス変更 => 配信中
            try
            {
                $status_running = \Model_Queue_Notification::QUEUE_STATUS_RUNNING;
                $q->status = $status_running;

                DB::start_transaction();
                if ( ! $q->save(false))
                {
                    throw new Exception('Notification queue status change failed. [news_id: '.$q->news_id.']');
                }
                DB::commit_transaction();
            }
            catch (Exception $e)
            {
                DB::rollback_transaction();
                Log::warning('Notification push failed.');
                continue;
            }

            // PUSH通知配信
            try
            {
                //$notice = new Notification($q->news_id);
                //$notice->push_notice();
                //Log::debug('Notification Send [new_id: '.$q->news_id);
            }
            catch (Exception $e)
            {
                $q->status = Model_Queue_Notification::QUEUE_STATUS_ERROR;
                $q->save(false);
                continue;
            }

            // キューステータス変更 => 配信完了
            try
            {
                $status_end = \Model_Queue_Notification::QUEUE_STATUS_END;
                $q->status = $status_end;

                DB::start_transaction();
                if ( ! $q->save(false))
                {
                    throw new Exception('Notification queue status change failed. [=> END] [news_id: '.$q->news_id.']');
                }
                DB::commit_transaction();
            }
            catch (Exception $e)
            {
                DB::rollback_transaction();
                Log::warning('Notification push failed. [news_id: '.$q->news_id.']');
                continue;
            }
        }
    }
}

/* End of file tasks/notification.php */
