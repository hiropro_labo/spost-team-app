{include "common/header.tpl"}


<div id="content"><!-- 固定ヘッダー+余白分 -->

<div class="ticket_main">

<p class="fc_w p1">各種販売方法に基づいて、<br>
チケットの購入方法を手続きにおすすめください。
</p>

<!-- 店頭販売 -->
<div class="shop fc_w">
    <p>店頭販売</p>
</div>

<!-- 購入方法 -->
<div class="buy_box">

    <div class="buy_title fc_w">
    <p>購入方法</p>
    </div>
    <!-- loppi -->
    <div class="fl">
        <div class="loppi"><img src="/assets/img/app/{$TEAM->id()}/ticket/mark_loppi.png" alt="Loppi" class="loppi_img" /></div>
    </div>
    <div class="fr">
        <div class="loppi_text">
        <p>お近くの<span class="fc_r">ローソン・ミニストップ</span>店内のLoppi（ロッピー）端末でご購入いただけます。<br>
        Lコードを入力または、キーワードで検索するか、「ジャンル」→「地域」→「ご希望の試合」を選択してください。<br>
        Loppiから出力された申込み券をレジにお持ちいただき、お支払・お受取りができます。<br>
        ★発券手数料　105円/枚</p>
    </div>
    <table class="loppi_tb">
    <tr>
    <td><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">ローソン</span></td>
    <td rowspan="2">発券手数料105円/枚</td>
    </tr>
    <tr>
    <td><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">ミニストップ</span></td>
    </tr>
    </table>

    </div>

    <hr class="clear">


    <!-- セブン -->
    <div class="fl">
        <div class="seven"><img src="/assets/img/app/{$TEAM->id()}/ticket/mark_seven.png" alt="seventicket" class="loppi_img" /></div><br>
    </div>
    <div class="fr">
        <div class="loppi_text">
            <p>マルチコピー機のタッチパネルで『セブンチケット』を選択してご購入いただけます。<br>
            印刷された払込票をレジにお持ちいただき、お支払・お受取りができます。</p>
        </div>
    </div>
    <div class="clear"></div>

    <!-- チケットぴあ -->
    <div class="fl">
        <div class="pia"><img src="/assets/img/app/{$TEAM->id()}/ticket/mark_pia.png" alt="チケットぴあ" class="loppi_img" /></div>
    </div>
    <div class="fr">
        <div class="loppi_text">
            <p>マルチコピー機のタッチパネルで『エンタメチケット』→『チケットぴあ』を選択してご購入いただけます。<br>
            Pコードまたはキーワードで検索・選択いただき、印刷された払込票をレジにお持ちいただき、お支払とお受取りができます。<br>
            ★発券手数料　なし</p>
        </div>
        <table class="loppi_tb">
        <tr>
        <td><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">セブンイレブン</span></td>
        <td>発券手数料なし</td>
        </tr>
        </table>
    </div>

    <hr class="clear">


    <!-- イープラス -->
    <div class="fl">
        <div class="eplus"><img src="/assets/img/app/{$TEAM->id()}/ticket/mark_eplas.png" alt="イープラス" class="loppi_img" /></div>
    </div>
    <div class="fr">
        <div class="loppi_text">
            <p>お近くのファミリーマート店内のファミポートでご購入いただけます。</p>
        </div>
        <table class="loppi_tb">
        <tr>
        <td><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">ファミリーマート</span></td>
        <td>発券手数料105円/枚</td>
        </tr>
        </table>
    </div>


    <div class="clear"></div>


</div><!-- (/buy_box) -->







<!------- インターネット販売 ------->
<div class="shop fc_w">
    <p>インターネット販売</p>
</div>

<!-- 購入方法 -->
<div class="buy_box">

    <div class="buy_title fc_w">
    <p>購入方法</p>
    </div>
    <!-- ローチケ -->
    <div class="fl">
        <div class="low"><img src="/assets/img/app/{$TEAM->id()}/ticket/mark_rowticke.png" alt="ローチケ" class="loppi_img" /></div>
    </div>
    <div class="fr">
        <div class="loppi_text">
        <p>会員登録（ＬＥncore会員またはモバイル会員またはローソンWEB会員）し、Lコードでご希望の試合を検索しご予約後、お近くのローソンまたはミニストップのLoppiにて、「予約済みチケットの引き取り」から予約番号等を入力し、Loppiから発券された申込み券にてお支払・お受取りができます。</p>
    </div>
    <table class="loppi_tb">
    <tr>
    <td><a href="http://l-tike.com/sports/bj/index.html"><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">ローソン</span></a></td>
    <td rowspan="2">315円<br>
    （システム利用料210円/枚<br>
        ＋発券手数料105円/枚）</td>
    </tr>
    <tr>
    <td><a href="http://l-tike.com/sports/bj/index.html"><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">ミニストップ</span></a></td>
    </tr>
    </table>

    </div>

    <hr class="clear">


    <!-- チケットぴあ -->
    <div class="fl">
        <div class="pia2"><img src="/assets/img/app/{$TEAM->id()}/ticket/mark_pia.png" alt="チケットぴあ" class="loppi_img" /></div><br>
    </div>
    <div class="fr">
        <div class="loppi_text">
            <p>会員登録し、オンライン画面でご予約ください。右記店舗での発券の際は、「払込票番号（13桁）」または「チケット引換番号（9桁）」とご予約時の電話番号下４桁が必要になります。</p>
        </div>
        <table class="loppi_tb">
        <tr>
        <td><a href="http://t2.pia.jp/sports/basketball/index.jsp"><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">セブンイレブン</span></a></td>
        <td rowspan="3">315円（システム利用料<br>
        210円/枚＋発券手数料<br>
        105円/枚）<br>
        ※ぴあプレミアム会員は<br>
        発券手数料が無料に<br>
        なります。</td>
        </tr>
        <tr>
        <td><a href="http://t2.pia.jp/sports/basketball/index.jsp"><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">ｻｰｸﾙKｻﾝｸｽ</span></a></td>
        </tr>
        <tr>
        <td><a href="http://t2.pia.jp/sports/basketball/index.jsp"><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">チケットぴあのお店</span></a></td>
        </tr>
        </table>

    </div>
    <hr class="clear">

    <!-- CNプレイガイド -->
    <div class="fl">
        <div class="cnplay"><img src="/assets/img/app/{$TEAM->id()}/ticket/mark_cn.png" alt="CNプレイガイド" class="loppi_img" /></div>
    </div>
    <div class="fr">
        <div class="loppi_text">
            <p>会員登録（CNプレ会員またはCNケータイ会員またはCNカード会員）をし、オンライン画面でご予約後、右記店舗にて予約番号を伝えお支払・お受取りができます。</p>
        </div>
        <table class="loppi_tb">
        <tr>
        <td><a href="http://www.cnplayguide.com/"><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">ファミリーマート</span></a></td>
        <td>発券手数料170円/枚</td>
        </tr>
        <tr>
        <td><a href="http://www.cnplayguide.com/"><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">セブンイレブン</span></a></td>
        <td>発券手数料170円/枚</td>
        </tr>
        <tr>
        <td><a href="http://www.cnplayguide.com/"><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">CNステーション</span></a></td>
        <td>手数料はかかりません</td>
        </tr>
        </table>
    </div>

    <hr class="clear">

    <!-- イープラス -->
    <div class="fl">
        <div class="eplus2"><img src="/assets/img/app/{$TEAM->id()}/ticket/mark_eplas.png" alt="イープラス" class="loppi_img" /></div>
    </div>
    <div class="fr">
        <div class="loppi_text">
            <p>会員登録し、オンライン画面でご予約後、右記店舗にてお支払・お受取りができます。</p>
        </div>
        <table class="loppi_tb">
        <tr>
        <td><a href="http://eplus.jp/sys/web/sports/basketball/index.html"><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">ファミリーマート</span></a></td>
        <td rowspan="2">315円<br>
        (システム利用料210円/<br>
        枚＋発券手数料105円/<br>
        枚）</td>
        </tr>
        <tr>
        <td><a href="http://eplus.jp/sys/web/sports/basketball/index.html"><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">セブンイレブン</span></a></td>
        </tr>
        <tr>
        <td><a href="http://eplus.jp/sys/web/sports/basketball/index.html"><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">配送</span></a></td>
        <td>配送の場合600円/<br>
        件のみ</td>
        </tr>
        </table>
    </div>
    <div class="clear"></div>

</div><!-- (/buy_box) -->




<!------- 電話販売 ------->
<div class="shop fc_w">
    <p>電話販売</p>
</div>

<!-- 購入方法 -->
<div class="buy_box">

    <div class="buy_title fc_w">
    <p>購入方法</p>
    </div>
    <!-- ローチケ -->
    <div class="fl">
        <div class="low2"><img src="/assets/img/app/{$TEAM->id()}/ticket/mark_rowticke.png" alt="ローチケ" class="loppi_img" /></div>
    </div>
    <div class="fr">
        <div class="loppi_text">
        <p>【自動応答システム】<br>
            TEL:0570-084-003<br>
            Lコードその他の情報をガイダンスに従って入力しご予約ください。世やう番号をお持ちになって、お近くのローソンにてお支払・お受取りができます。
            <br>
            【オペレータ対応】<br>
            TEL 0570-02-9999<br>
            オペレータにご希望の試合日を伝えご予約ください。予約番号をお持ちになって、お近くのローソンにてお支払・お受取りができます。</p>
    </div>
    <table class="loppi_tb">
    <tr>
    <td><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">ローソン</span></td>
    <td>315円<br>
    （システム利用料210円/枚<br>
    　＋発券手数料105円/枚）</td>
    </tr>
    </table>

    </div>

    <hr class="clear">


    <!-- チケットぴあ -->
    <div class="fl">
        <div class="pia3"><img src="/assets/img/app/{$TEAM->id()}/ticket/mark_pia.png" alt="チケットぴあ" class="loppi_img" /></div><br>
    </div>
    <div class="fr">
        <div class="loppi_text">
            <p>TEL:0570-02-9999<br>
            Pコード（593-002）を入力、または会場名・公演名を発話 → ガイダンスに従ってご予約ください。右記店舗での発券の際は、「払込票番号（13桁）」または「チケット引換番号（9桁）」とご予約時の電話番号下４桁が必要になります。</p>
        </div>
        <table class="loppi_tb">
        <tr>
        <td><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">セブンイレブン</span></td>
        <td rowspan="3">315円<br>
        （システム利用料210円/<br>
        枚＋発券手数料105円/<br>
        枚）<br>
        ※ぴあプレミアム会員は<br>
         発券手数料が<br>
        無料になります。</td>
        </tr>
        <tr>
        <td><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">ｻｰｸﾙKｻﾝｸｽ</span></td>
        </tr>
        <tr>
        <td><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">ぴあおのお店</span></td>
        </tr>
        </table>

    </div>
    <hr class="clear">

    <!-- CNプレイガイド -->
    <div class="fl">
        <div class="cnplay"><img src="/assets/img/app/{$TEAM->id()}/ticket/mark_cn.png" alt="CNプレイガイド" class="loppi_img" /></div>
    </div>
    <div class="fr">
        <div class="loppi_text">
            <p>TEL:0570-08-9999<br>
            （10：00-18：00，オペレータ対応）<br>
            オペレータにご希望の試合日を伝えご予約ください。予約番号をお持ちになって、お近くのセブン-イレブン、またはファミリーマートにてお支払・お受取りができます。</p>
        </div>
        <table class="loppi_tb">
        <tr>
        <td><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">ファミリーマート</span></td>
        <td>発券手数料170円/枚</td>
        </tr>
        <tr>
        <td><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">セブンイレブン</span></td>
        <td>発券手数料170円/枚</td>
        </tr>
        <tr>
        <td><span><img src="/assets/img/app/{$TEAM->id()}/ticket/arrow_icon_gray.png" alt=">>" class="ticket_black_arrow" /></span><span class="ticket_tb_text">CNステーション</span></td>
        <td>手数料はかかりません</td>
        </tr>
        </table>
    </div>

    <div class="clear"></div>

</div><!-- (/buy_box) -->


<div class="pb_10"></div>





</div><!-- (/ticket_main) -->

{include "common/footer.tpl"}