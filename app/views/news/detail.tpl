{include "common/header.tpl"}

<div id="content"><!-- 固定ヘッダー+余白分 --><!-- NEWS -->

<div id="news_contents">
    <div id="title_bar" class="title scm_text_li">
    	<span class="title_txt">{$news->title}</span><br />
    	<span class="title_day">{$news->updated_at|date_format:"%Y年%m月%d日 %H:%M"}</span>
    	{if $news->good == 0}
    	<div id="good_contents"><img id="likeButtonImage" src="/assets/img/common/btn_good_on.png" onClick="sendGood('{$token}', '{$TEAM->id()}', '{$news->id}')"/><span class="count">{$news->good_cnt}</span></div>
    	{else}
    	<div id="good_contents"><img src="/assets/img/common/btn_good_off.png" /><span>{$news->good_cnt}</span></div>
    	{/if}
    </div>

    <div class="detail">{$news->body|nl2br}</div>

    {if !empty($news->file_name)}
    <div class="photo"><img src="http://timg.spost.jp/app/news/{$TEAM->id()}/{$news->file_name}" style="width: 95%; height: auto;"/></div>
    {/if}
</div><!-- (/sub_contents) -->

<div class="clear"></div>

</div><!-- (/#content) -->


{include "common/footer.tpl"}
