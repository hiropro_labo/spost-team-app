<?php
/**
 * Model Team Member: チアメンバー
 *
 * @author     Kouji Itahana
 */

class Model_Cheer_Member extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'CHEER_MEMBER';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'cheer_member';

    /**
     * @var string
     */
    protected static $_user_id_column = 'client_id';

    /**
     * @var array datetime columns
     */
    protected static $_datetime_columns = array(
        'updated_at', 'created_at'
    );

    /**
     * 表示順にソートされたリストを取得
     *
     * @param string $id
     * @return Array|NULL
     */
    public static function get_member_list_all($id)
    {
        $list = self::find_by(array(static::$_user_id_column => $id));
        if ( ! is_null($list))
        {
            usort($list, function($a, $b){
                return $a['position'] > $b['position'];
            });
        }

        return $list;
    }

    /**
     * 表示順にソートされたリストを取得（※表示設定が有効もののみ）
     *
     * @param string $id
     * @return boolean|unknown
     */
    public static function get_member_list($id)
    {
        $list = self::find_by(array(
            static::$_user_id_column => $id,
            'enable'    => 1
        ));
        if ( ! is_null($list))
        {
            usort($list, function($a, $b){
                return $a['position'] > $b['position'];
            });
        }

        return $list;
    }
}