<?php
/**
 * The staging database settings. These get merged with the global settings.
 */

return array(
    //-------------------------------------
    // Redis
    //-------------------------------------
    'redis' => array(
        'default' => array(
            'hostname' => '127.0.0.1',
            'port'     => '6379',
        ),
    ),

    //-------------------------------------
    // MAIN
    //-------------------------------------
	'default' => array(
        'type'         => 'pdo',
        'identifier'   => '`',
        'table_prefix' => '',
        'charset'      => 'utf8',
        'enable_cache' => true,
        'profiling'    => false,
        'connection'   => array(
            'dsn'        => 'mysql:host=localhost;dbname=spost_main_staging',
            'username'   => 'HIRO_spost',
            'password'   => 'mL66eprtCmF7azAP',
        ),
	),

    //-------------------------------------
    // TEAM: team
    //-------------------------------------
    'team' => array(
        'type'         => 'pdo',
        'identifier'   => '`',
        'table_prefix' => '',
        'charset'      => 'utf8',
        'enable_cache' => true,
        'profiling'    => false,
        'connection'   => array(
            'dsn'        => 'mysql:host=localhost;dbname=spost_team_staging',
            'username'   => 'HIRO_spost',
            'password'   => 'mL66eprtCmF7azAP',
        ),
    ),

    //-------------------------------------
    // SPONSOR: s0001
    //-------------------------------------
    's0001' => array(
        'type'         => 'pdo',
        'identifier'   => '`',
        'table_prefix' => '',
        'charset'      => 'utf8',
        'enable_cache' => true,
        'profiling'    => false,
        'connection'   => array(
            'dsn'        => 'mysql:host=localhost;dbname=spost_s0001_staging',
            'username'   => 'HIRO_spost',
            'password'   => 'mL66eprtCmF7azAP',
        ),
    ),

    //-------------------------------------
    // SPONSOR: チームアプリユーザー
    //-------------------------------------
    'ut0001' => array(
        'type'         => 'pdo',
        'identifier'   => '`',
        'table_prefix' => '',
        'charset'      => 'utf8',
        'enable_cache' => true,
        'profiling'    => false,
        'connection'   => array(
            'dsn'        => 'mysql:host=localhost;dbname=spost_ut0001_staging',
            'username'   => 'HIRO_ipost',
            'password'   => 'mL66eprtCmF7azAP',
        ),
    ),

    //-------------------------------------
    // SPONSOR: スポンサーアプリユーザー
    //-------------------------------------
    'us0001' => array(
        'type'         => 'pdo',
        'identifier'   => '`',
        'table_prefix' => '',
        'charset'      => 'utf8',
        'enable_cache' => true,
        'profiling'    => false,
        'connection'   => array(
            'dsn'        => 'mysql:host=localhost;dbname=spost_us0001_staging',
            'username'   => 'HIRO_ipost',
            'password'   => 'mL66eprtCmF7azAP',
        ),
    ),

    //-------------------------------------
    // TEAM: チームアプリ ユーザートークン
    //-------------------------------------
    'tt0001' => array(
        'type'         => 'pdo',
        'identifier'   => '`',
        'table_prefix' => '',
        'charset'      => 'utf8',
        'enable_cache' => true,
        'profiling'    => false,
        'connection'   => array(
            'dsn'        => 'mysql:host=localhost;dbname=spost_tt0001_staging',
            'username'   => 'HIRO_spost',
            'password'   => 'mL66eprtCmF7azAP',
        ),
    ),

    //-------------------------------------
    // SPONSOR: アプリユーザートークン
    //-------------------------------------
    'ts0001' => array(
        'type'         => 'pdo',
        'identifier'   => '`',
        'table_prefix' => '',
        'charset'      => 'utf8',
        'enable_cache' => true,
        'profiling'    => false,
        'connection'   => array(
            'dsn'        => 'mysql:host=localhost;dbname=spost_ts0001_staging',
            'username'   => 'HIRO_ipost',
            'password'   => 'mL66eprtCmF7azAP',
        ),
    ),

    //-------------------------------------
    // Payment: p0001
    //-------------------------------------
    'p0001' => array(
        'type'         => 'pdo',
        'identifier'   => '`',
        'table_prefix' => '',
        'charset'      => 'utf8',
        'enable_cache' => true,
        'profiling'    => false,
        'connection'   => array(
            'dsn'        => 'mysql:host=localhost;dbname=spost_p0001_staging',
            'username'   => 'HIRO_spost',
            'password'   => 'mL66eprtCmF7azAP',
        ),
    ),

);
