<?php
use Fuel\Core\Model_Crud;

/**
 * Model チームステータス
 *
 * @author     Kouji Itahana
 */

class Model_Master_Team_Status extends Model_Crud_Master
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'M_TEAM_STATUS';

    /**
     * Select用Array生成
     *
     * @return array
     */
    public static function list_opt_array()
    {
        $opt = array('' => '----- ステータスを選択 -----');
        $list = self::find_all();
        foreach ($list as $entry)
        {
            $opt[$entry->id] = $entry->comment.'('.$entry->name.')';
        }
        return $opt;
    }

    /**
     * ステータス名称取得
     *
     * @return string
     */
    public static function status_name($status)
    {
        $rec = self::find_one_by('id', $status);
        if (is_null($rec))
        {
            return null;
        }

        return $rec->comment.'('.$rec->name.')';
    }
}