<?php
use Fuel\Core\Model_Crud;

/**
 * Model 購入エントリー
 *
 * ・購入商品の確認ページでレコード生成するので未決済のエントリも含まれる
 * ・購入ステータスはM_PAYMENT_STATUSテーブル
 *
 * @author     Kouji Itahana
 */

class Model_Payment_Error_Kickback extends Model_Crud_Shard_Payment
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'PAYMENT_ERROR_KICKBACK';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

}