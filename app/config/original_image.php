<?php
/**
 * 画像処理の設定値
 */
return array(
    /**
     * アプリ参照画像：保存先ルートディレクトリパス
     */
    'app_img_root' => array(
        // チーム
        'shop'            => '/assets/img/app/shop/',
        // TOP: スライド画像
        'top_recommend'   => '/assets/img/app/top/',
        // COUPON: クーポン画像
        'coupon'          => '/assets/img/app/coupon/',
        // TEAM: トップ画像
        'team_top'        => '/assets/img/app/team/top/',
        // TEAM: メンバー画像
        'team_member'     => '/assets/img/app/team/member/',
        // CHEER: トップ画像
        'cheer_top'       => '/assets/img/app/cheer/top/',
        // CHEER: チア画像
        'cheer_member'    => '/assets/img/app/cheer/member/',
        // MENU: トップ画像
        'menu_top'        => '/assets/img/app/menu/top/',
        // MENU: カテゴリー画像
        'menu_category'   => '/assets/img/app/menu/cate/',
        // MENU: 商品詳細画像
        'menu_item'       => '/assets/img/app/menu/item/',
        // NEWS
        'news'            => '/assets/img/app/news/',
        // ICON
        'icon'            => '/assets/img/app/store/',
        // スクーンショット１
        'screen_shot1'    => '/assets/img/app/screen_shot1/',
        // スクーンショット１
        'screen_shot2'    => '/assets/img/app/screen_shot2/',
        // スプラッシュ画像
        'splash'          => '/assets/img/app/splash/',
        // 支店
        'branch'          => '/assets/img/app/branch/',
        // SCHEDULE: トップ画像
        'schedule_top'    => '/assets/img/app/schedule/top/',
        // RESULT: トップ画像
        'result_top'      => '/assets/img/app/result/top/',
        // GALLERY: ギャラリー画像
        'gallery'         => '/assets/img/app/gallery/',
        ),

    /**
     * imageパス: common
     */
    'img_common_path' => '/assets/img/common/',

    /**
     * アップロード画像保存先ディレクトリ
     * ※実際には以下のパス配下のクライアントIDのディレクトリ配下へ画像が保存される
     * ・・・/shop/[client-id]/◯◯◯.jpg
     */
    'save_dir' => array(
        // ショップ
        'shop'            => DOCROOT.'assets/img/app/shop/',
        // TOP: スライド画像
        'top_recommend'   => DOCROOT.'assets/img/app/top/',
        // COUPON: クーポン画像
        'coupon'          => DOCROOT.'assets/img/app/coupon/',
        // TEAM: トップ画像
        'team_top'        => DOCROOT.'assets/img/app/team/top/',
        // TEAM: メンバー画像
        'team_member'     => DOCROOT.'assets/img/app/team/member/',
        // CHEER: トップ画像
        'cheer_top'       => DOCROOT.'assets/img/app/cheer/top/',
        // CHEER: メンバー画像
        'cheer_member'    => DOCROOT.'assets/img/app/cheer/member/',
        // MENU: トップ画像
        'menu_top'        => DOCROOT.'assets/img/app/menu/top/',
        // MENU: カテゴリー画像
        'menu_category'   => DOCROOT.'assets/img/app/menu/cate/',
        // MENU: 商品詳細画像
        'menu_item'       => DOCROOT.'assets/img/app/menu/item/',
        // NEWS
        'news'            => DOCROOT.'assets/img/app/news/',
        // ICON
        'icon'            => DOCROOT.'assets/img/app/icon/',
        // 支店
        'branch'          => DOCROOT.'assets/img/app/branch/',
        // SCHEDULE: トップ画像
        'schedule_top'    => DOCROOT.'assets/img/app/schedule/top/',
        // RESULT: トップ画像
        'result_top'      => DOCROOT.'assets/img/app/result/top/',
        // GALLERY: ギャラリー画像
        'gallery'         => DOCROOT.'assets/img/app/gallery/',
        ),

    /**
     * 各種ブランク画像ファイル名
     * ※保存先ディレクトリ：img_common_path直下
     */
    'img_blank' => array(
        'shop'            => 'noimage/noimage_top_shop.png',
        'top'             => 'noimage/noimage_top_shop.png',
        'top_recommend'   => 'noimage/noimage_top_shop.png',
        'coupon'          => 'noimage/noimage_coupon.png',
        'team_top'        => 'noimage/noimage_team_top.png',
        'team_member'     => 'noimage/noimage_team_member.png',
        'cheer_top'       => 'noimage/noimage_cheer_top.png',
        'cheer_member'    => 'noimage/noimage_cheer_member.png',
        'menu_top'        => 'noimage/noimage_menutop.png',
        'menu_category'   => 'noimage/noimage_menu_news.png',
        'menu_item_thumb' => 'noimage/noimage_menu_news.png',
        'menu_item'       => 'noimage/noimage_menu_news.png',
        'news'            => 'noimage/noimage_menu_news.png',
        'icon'            => 'noimage/noimage_icon.png',
        'screen_shot1'    => 'noimage/noimage_screen_shot.png',
        'screen_shot2'    => 'noimage/noimage_screen_shot2.png',
        'splash'          => 'noimage/noimage_splash.png',
        'branch'          => 'noimage/noimage_top_shop.png',
        'schedule_top'    => 'noimage/noimage_schedule_top.png',
        'result_top'      => 'noimage/noimage_result_top.png',
        'gallery'         => 'noimage/noimage_gallery.png',
    ),

);

