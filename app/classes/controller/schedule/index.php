<?php
/**
 * Schedule
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Schedule_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'schedule/';
    /**
     * action 前処理
     */
    protected function before_controller()
    {
        //static::$_client_id = $this->setup_shop();
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

	    $image = self::$_team->contents()->schedule_top()->model();
	    $list  = self::$_team->contents()->schedule()->model_list();

	    $view->set('list',  $list);
	    $view->set('image', $image);
	    return $view;
	}

	/**
	 * Detail
	 *
	 * @access public
	 * @return Response
	 */
	public function action_detail()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'detail.tpl');

	    $id = $this->param('id');
	    if (empty($id))
	    {
	        Response::redirect(404);
	    }
	    $schedule = self::$_team->contents()->schedule()->model_by_id($id);

	    $view->set('schedule', $schedule);
	    return $view;
	}
}
