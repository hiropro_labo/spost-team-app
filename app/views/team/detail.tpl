{include "common/header_member_detail.tpl"}

<div id="content"><!-- 固定ヘッダー+余白分 -->
<div id="team_member_detail">
  <img src="http://timg.spost.jp/app/team/member/{$TEAM->id()}/{$member->file_name}" class="w_100">

  <!--item-->
  <div class="item">
    <table>
      <tr><td class="td">ポジション</td><td>{$member->player_position}</td></tr>
      <tr><td class="td">生年月日</td><td>{$member->birth_year}年{$member->birth_month}月{$member->birth_date}日</td></tr>
      <tr><td class="td">身長</td><td>{$member->height}cm</td></tr>
      <tr><td class="td">体重</td><td>{$member->weight}kg</td></tr>
      <tr><td class="td">出身地</td><td>{$member->alma_mater}</td></tr>
    </table>
  </div>
  <!--/item-->

</div>

{include "common/footer.tpl"}