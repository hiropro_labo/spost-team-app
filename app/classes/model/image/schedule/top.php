<?php
/**
 * Model：スケジュールトップ画像情報
 *
 * @author     Kouji Itahana
 */

class Model_Image_Schedule_Top extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_SCHEDULE_TOP';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'schedule_top';

    /**
     * カラム[link]がある場合はtrue
     * @var bolean
     */
    protected static $_column_link = false;
}