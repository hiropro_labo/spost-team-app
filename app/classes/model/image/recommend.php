<?php
/**
 * Model TOPページ：スライド画像
 *
 * @author     Kouji Itahana
 */

class Model_Image_Recommend extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_RECOMMEND';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'top_recommend';

}