<?php
/**
 * Model News: ニュース情報
 *
 * @author     Kouji Itahana
 */

class Model_News_Info extends Model_Crud_Shard_Image
{
    /**
     * PUSH通知配信フラグ：通知なし
     * @var int
     */
    const NOTICE_SETTING_OFF = 0;

    /**
     * PUSH通知配信フラグ：通知なし
     * @var int
     */
    const NOTICE_SETTING_ON  = 1;

    /**
     * PUSH通知配信完了フラグ：未配信
     * @var int
     */
    const NOTICE_STATUS_OFF = 0;

    /**
     * PUSH通知配信完了フラグ：配信済
     * @var int
     */
    const NOTICE_STATUS_ON  = 1;

    /**
     * @var string
     */
    protected static $_user_id_column = 'client_id';

    /**
    * @var  string  $_table_name  The table name
    */
    protected static $_table_name = 'NEWS_INFO';

    /**
    * @var  string  fieldname of created_at field, uncomment to use.
    */
    protected static $_created_at = 'created_at';

    /**
    * @var  string  fieldname of updated_at field, uncomment to use.
    */
    protected static $_updated_at = 'updated_at';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'news';

    protected static $_datetime_columns = array(
        'updated_at', 'created_at', 'send_at'
    );

    /**
    * ニュース削除（※画像があれば画像も削除)
    *
    * @param string $client_id
    * @param string $id
    * @throws Exception
    * @return boolean
    */
    public static function delete_news($client_id, $id)
    {
        try
        {
            $news = self::find_one_by(array('id' => $id, 'client_id' => $client_id));
            if (is_null($news))
            {
                throw new Exception('news is not found.');
            }
            $news->delete();

            $conf_image = \Config::load('original_image', true);
            $save_dir   = $conf_image['save_dir']['news'].$client_id.DS;
            $file_name  = $news->file_name;

            if (file_exists($save_dir.$file_name))
            {
                File::delete($save_dir.$file_name);
            }
        }
        catch (Exception $e)
        {
            return false;
        }

        return true;
    }

    /**
    * ニュースIDから画像ファイル名を生成
    *
    * @param string $position
    * @return mixed
    */
    public static function create_file_name($id)
    {
        $conf_upload = \Config::load('original_upload', true);
        $conf_upload = $conf_upload['news'];
        $file_name   = $conf_upload['suffix'];
        $file_name   = str_replace('%%%img_no%%%', $id, $file_name);
        return $file_name;
    }
}