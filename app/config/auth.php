<?php
/**
 * Auth 設定ファイル
 *
 * ・app/config配下へコピーして仕様
 * ・以下の設定項目は全て必須項目です
 *
 * [ ユーザテーブル ]
 * 必須カラム: login_id, password, last_login, login_hash
 * テーブル名: 任意の名前
 */

return array(
    /**
     * ログイン認証：ユーザテーブル名
     */
    'user_table' => 'USER_CLIENT',

    /**
     * ログイン認証：テーブルカラム名 -> ユーザーID
     */
    'column_user_id' => 'id',

    /**
     * ログイン認証：テーブルカラム名 -> ログインID
     */
    'column_login_id' => 'login_id',

    /**
     * ログイン認証：テーブルカラム名 -> パスワード
     */
    'column_password' => 'password',

    /**
     * ログイン認証：テーブルカラム名 -> ログインハッシュ
     */
    'column_login_hash' => 'login_hash',

    /**
     * ログイン認証：フォームPOSTキー -> ログインID
     */
    'post_login_id' => 'login_id',

    /**
     * ログイン認証：フォームPOSTキー -> パスワード
     */
    'post_password' => 'password',

    /**
     * Session 保存キー: ユーザーID
     */
    'session_user_id' => 'user_id',

    /**
     * Session 保存キー: ログインID
     */
    'session_login_id' => 'login_id',

    /**
     * Session 保存キー: ログインハッシュ
     */
    'session_login_hash' => 'login_hash',

    /**
     * Login Hash-Salt
     */
    'login_hash_salt' => 'UVTUMDRst2KfzPuSKPPfrgGAe6cyKZBT',
);
