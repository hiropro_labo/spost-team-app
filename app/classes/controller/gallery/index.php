<?php
/**
 * GALLERY
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Gallery_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'gallery/';
    /**
     * action 前処理
     */
    protected function before_controller()
    {
        //static::$_client_id = $this->setup_shop();
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

	    $list = self::$_team->contents()->gallery()->list_image();

	    $view->set('list',  $list);
	    return $view;
	}

    /**
	 * DETAIL
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_detail()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'detail.tpl');

	    $id = $this->param('id');
	    if (empty($id))
	    {
	        Response::redirect(404);
	    }
	    $image = self::$_team->contents()->gallery()->model_by_id($id);

	    $view->set('image', $image);
	    return $view;
	}
}
