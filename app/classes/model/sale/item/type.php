<?php
use Fuel\Core\Model_Crud;

/**
 * Model 販売商品タイプマスター
 *
 * @author     Kouji Itahana
 */

class Model_Master_Sale_Item_Type extends Model_Crud
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'M_SALE_ITEM_TYPE';

    /**
     * Select用Array生成
     *
     * @return array
     */
    public static function list_opt_array()
    {
        $opt = array('' => '----- 商品タイプを選択 -----');
        $list = self::find_all();
        foreach ($list as $entry)
        {
            $opt[$entry->id] = $entry->name;
        }
        return $opt;
    }

    /**
     * タイプ名称取得
     *
     * @param string id
     * @return string
     */
    public static function get_type_name($id)
    {
        $item = self::find_by_pk($id);
        return isset($item['name']) ? $item['name'] : '';
    }

}