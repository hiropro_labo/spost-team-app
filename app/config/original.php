<?php
/**
 * 各種設定ファイル
 *
 * ・アプリ内の各所で利用する汎用的な設定項目は本ファイルへ記述
 * ・config.phpと分ける目的で本ファイルを設置
 * ・設定内容に特に決まりはないがパッケージ化したものなどはそちらの専用ファイルへ記述
 */

return array(
    /**
     * ログインページURL(※相対パス)
     */
    'login_url_path' => 'auth/login',

    /**
     * DB: Deleteフラグ値
     */
    'delete_flg' => 1,

    /**
     * imageパス
     */
    'img_path' => '/assets/img/',

    /**
     * cssパス
     */
    'css_path' => '/assets/css/',

    /**
     * jsパス
     */
    'js_path' => '/assets/js/',

    /**
     * TOPスライド画像：最大エントリ数
     */
    'top_image_max' => 5,

    /**
     * COUPN画像：最大エントリ数
     */
    'coupon_image_max' => 5,

    /**
     * ユーザー登録先Shard指定
     */
    'regist_shard' => 'u0001',

    /**
     * トークン登録先Shard指定
     */
    'regist_shard_token' => 't0001',

    /**
     * メンテナンス：スイッチ
     *
     * true:有効/false:無効
     * ※メンテナンスモードにするには本スイッチをtrueにした上で
     * maintenance_[start|end]_datetimeで期間指定する
     */
    'maintenance_switch' => false,

    /**
     * メンテナンス期間：開始日時
     */
    'maintenance_start_datetime' => '2013-10-15 00:00:00',

    /**
     * メンテナンス期間：終了日時
     */
    'maintenance_end_datetime'   => '2013-10-25 23:59:59',

    /**
     * ショップURL
     */
    'default_shop_url' => 'http://i-post.jp/',

    /**
     * オンラインショップURL
     */
    'default_online_shop_url' => 'http://i-post.jp/',

    /**
     * 有効期限猶予：日数
     *
     * ※有効期限が切れても有効期限から指定日数の間はアプリ停止しない
     * 事実上、有効期限が指定日数伸びることになる
     */
    'delay_day' => 10,

);
