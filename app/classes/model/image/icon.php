<?php
/**
 * Model アプリアイコン画像
 *
 * @author     Kouji Itahana
 */

class Model_Image_Icon extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'IMAGE_ICON';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'icon';

    /**
     * カラム[link]がある場合はtrue
     * @var bolean
     */
    protected static $_column_link = false;
}