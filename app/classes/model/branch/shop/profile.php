<?php
/**
 * Model 支店データ
 *
 * @author     Kouji Itahana
 */

class Model_Branch_Shop_Profile extends Model_Crud_Shard
{
    /**
     * 表示設定：非表示
     * @var number
     */
    const ENABLE_FLG_OFF = 0;

    /**
     * 表示設定：表示
     * @var number
     */
    const ENABLE_FLG_ON  = 1;

    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'BRANCH_SHOP_PROFILE';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * 表示順にソートされたリストを取得
     *
     * @param string $client_id
     * @return Array|NULL
     */
    public static function get_category_list_all($client_id)
    {
        $list = self::find_by(array('client_id' => $client_id));
        if ( ! is_null($list))
        {
            usort($list, function($a, $b){
                return $a['position'] > $b['position'];
            });
        }

        return $list;
    }

    /**
     * 表示順にソートされたリストを取得（※表示設定のもののみ）
     *
     * @param string $client_id
     * @return boolean|unknown
     */
    public static function get_category_list($client_id)
    {
        $list = self::find_by(array(
            'client_id' => $client_id,
            'enable'    => 1
        ));

        if ( ! is_null($list))
        {
            usort($list, function($a, $b){
                return $a['position'] > $b['position'];
            });
        }

        return $list;
    }

    /**
     * Imageフルパス取得
     *
     * @return string image full path
     */
    public function image_path()
        {
        if (is_null($this->file_name) or empty($this->file_name))
        {
            return $this->blank_image();
        }
        $conf = \Config::load('original_image', true);
        $c_id = $this->client_id;
        $save_dir = $conf['app_img_root']['branch'].$c_id.DS;
        return $save_dir.$this->file_name;
    }

    /**
     * 一時Imageフルパス取得
     *
     * @return string image full path
     */
    public function tmp_image_path($client_id)
    {
        $c_id          = $client_id;
        $tmp_file_name = null;

        $file_upload = new \Support\File_Upload($c_id, 'branch');
        $tmp_files   = $file_upload->get_tmp_files();
        if ( ! empty($tmp_files))
        {
            $tmp_file_name = basename(array_shift($tmp_files));
        }

        if (is_null($tmp_file_name))
        {
            return $this->blank_image();
        }

        $conf = \Config::load('original_image', true);
        $save_dir = $conf['app_img_root']['branch'].$c_id.DS;
        return $save_dir.$tmp_file_name;
    }

    /**
     * ブランク画像ファイルパス取得
     *
     * @return string blank-img-name
     */
    public function blank_image()
    {
        $conf = \Config::load('original_image', true);
        return $conf['img_common_path'].$conf['img_blank']['branch'];
    }

    /**
     * TOP画像表示: 順序変更 順序ダウン
     *
     * @param string $client_id
     * @param string $current_position
     * @param int    $max
     * @throws Exception
     * @return boolean
     */
    public static function position_down($client_id, $current_position, $max)
    {
        try
        {
            DB::start_transaction();

            $image = self::find_one_by(array(
                'client_id' => $client_id,
                'position'  => $current_position
            ));
            if (is_null($image))
            {
                throw new Exception('Error: branch position change failed.');
            }

            $new_position = null;
            if ($image->position < $max)
            {
                $new_position = $image->position + 1;
            }
            else
            {
                throw new Exception('Error: branch position change failed.');
            }

            $image_other = self::find_one_by(array(
                'client_id' => $client_id,
                'position'  => $new_position
            ));
            if (! is_null($image_other))
            {
                $image_other->position  = $image->position;
                if (! $image_other->save(false))
                {
                    throw new Exception('Error: branch position change failed.');
                }
            }

            $image->position = $new_position;
            if (! $image->save(false))
            {
                throw new Exception('Error: branch position change failed.');
            }

            DB::commit_transaction();
        }
        catch(Exception $e)
        {
            DB::rollback_transaction();
            throw new Exception('Error: branch position change failed.');
        }

        return true;
    }

    /**
     * TOP画像表示: 順序変更 順序アップ
     *
     * @param string $client_id
     * @param string $current_position
     * @param int    $max
     * @throws Exception
     * @return boolean
     */
    public static function position_up($client_id, $current_position)
    {
        try
        {
            DB::start_transaction();

            $image = self::find_one_by(array(
                'client_id' => $client_id,
                'position'  => $current_position
            ));
            if (is_null($image))
            {
                throw new Exception('Error: branch position change failed.');
            }

            $new_position = null;
            if ($current_position > 1)
            {
                $new_position = $image->position - 1;
            }

            $image_other = self::find_one_by(array(
                'client_id' => $client_id,
                'position'  => $new_position
            ));
            if (! is_null($image_other))
            {
                $image_other->position = $image->position;
                if (! $image_other->save(false))
                {
                    throw new Exception('Error: branch position change failed.');
                }
            }

            $image->position  = $new_position;
            if (! $image->save(false))
            {
                throw new Exception('Error: branch position change failed.');
            }

            DB::commit_transaction();
        }
        catch(Exception $e)
        {
            DB::rollback_transaction();
            throw new Exception('Error: branch position change failed.');
        }

        return true;
    }

    /**
     * ポジションを再セット（※エントリ削除時などに使用）
     */
    public static function recreate_position($client_id)
    {
        $list = self::find_by('client_id', $client_id);
        if (is_null($list))
        {
            return;
        }
        else
        {
            usort($list, function($a, $b){
                return $a['position'] > $b['position'];
            });
        }

        $idx = 1;
        foreach ($list as $entry)
        {
            $entry->position = $idx;
            if ( ! $entry->save(false))
            {
                throw new Exception('recreate position error.');
            }
            $idx++;
        }
    }

    /**
     * カテゴリーポジション（※新規追加時のポジション取得）
     *
     * @param string $client_id
     * @return number
     */
    public function new_position($client_id)
    {
        $cnt = self::count('id', null, array('client_id' => $client_id));
        return $cnt + 1;
    }

    /**
     * Select用Array生成
     *
     * @return array
     */
    public static function list_opt_array($client_id)
    {
        $opt = array('' => '----- カテゴリー選択 -----');
        $list = self::find_by(array('client_id' => $client_id));
        foreach ($list as $entry)
        {
            $opt[$entry->id] = $entry->title;
        }
        return $opt;
    }

}