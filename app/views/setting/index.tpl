{include "common/header_setting.tpl"}

<div id="info">

  <h1><i class="fa fa-info-circle"></i>Info</h1>

  {if !is_null($notif)}
  <h2>Push通知設定</h2>
  <p class="not_link">アプリからのお知らせ
    <input id="flg" type="checkbox" value="1" {if $notif == 1}checked{/if}>
  </p>
  {/if}

  <h2>このアプリについて</h2>
  <p><a href="#">アプリの使い方<i class="fa fa-angle-right"></i></a></p><hr>
  <p><a href="#">よくある質問<i class="fa fa-angle-right"></i></a></p><hr>
  <p><a href="#">お問い合わせ<i class="fa fa-angle-right"></i></a></p><hr>

</div>

{literal}
<script type="text/javascript">
{/literal}
var token   = '{$token}';
var team_id = {$TEAM->id()};
{literal}
$(function(){
	var flg = 1;
    $('#flg').change(function(){
        if($(this).is(':checked')){
        	flg = 1;
        }else{
        	flg = 0;
        }

        var data = {
        		team_id: team_id,
                token: token,
                flg: flg
            };

        $.ajax({
            type: "post",
            url:  "/setting/notif/",
            contentType: "application/json",
            dataType: 'json',
            data: JSON.stringify(data),
            success: function(res){
                if (!res[0]){
                    alert("更新処理に失敗しました\n" + res[1]);
                }else{
                    location.reload();
                }
            },
            error: function(){
                alert("更新処理に失敗しました[通信エラー発生]");
            },
            complete: function(){
                button.attr("disabled", false);
            }
        });
    });
});
</script>
{/literal}

{include "common/footer.tpl"}