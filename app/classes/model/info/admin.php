<?php
/**
 * Model: iPost Admin 運営からのお知らせ
 *
 * @author     Kouji Itahana
 */

class Model_Info_Admin extends Model_Crud
{
  /**
   * @var  string  $_table_name  The table name
   */
  protected static $_table_name = 'INFO_ADMIN';

  /**
   * @var  string  fieldname of created_at field, uncomment to use.
   */
  protected static $_created_at = 'created_at';

  /**
   * @var  string  fieldname of updated_at field, uncomment to use.
   */
  protected static $_updated_at = 'updated_at';

  /**
   * Finds all records in the table.  Optionally limited and offset.
   *
   * @param   int     $limit     Number of records to return
   * @param   int     $offset    What record to start at
   * @return  null|object        Null if not found or an array of Model object
   */
  public static function find_all($limit = null, $offset = 0)
  {
      $config = array('limit' => $limit, 'offset' => $offset, 'order_by' => array('created_at' => 'desc'));
      // $alive ? $config['where'] = array(array('del', '=', 0)) : null;
    return static::find($config);
  }
}