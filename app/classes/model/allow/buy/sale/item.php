<?php
/**
 * Model 販売アイテム許可リスト
 *
 * @author     Kouji Itahana
 */

class Model_Allow_Buy_Sale_Item extends Model_Crud
{
    /**
     * 表示設定：非表示
     * @var number
     */
    const ENABLE_FLG_OFF = 0;

    /**
     * 表示設定：表示
     * @var number
     */
    const ENABLE_FLG_ON  = 1;

    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'ALLOW_BUY_SALE_ITEM';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

}