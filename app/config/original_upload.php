<?php
/**
 * ファイルアップロード設定ファイル
 * ※Upload::processへセットする為の設定用Arrayを定義
 */
return array(
    /*
     * ショップ画像用
     */
    'shop' => array(
        // アップ画像一時保存先
        'path'          => APPPATH.'tmp/up_tmp/',
        // リネームする際のファイル名
        'suffix'        => 'shop_',
        // 保存する際のリネーム
        'auto_rename'   => true,
        // 許可する拡張子
        'ext_whitelist' => array('png', 'jpg', 'jpeg', 'gif'),
    ),

    /*
     * TOPカルーセル画像
     */
    'top_recommend' => array(
        // アップ画像一時保存先
        'path'          => APPPATH.'tmp/up_tmp/',
        // リネームする際のファイル名
        'suffix'        => 'top_%%%img_no%%%.jpg',
        // 保存する際のリネーム
        'auto_rename'   => true,
        // 許可する拡張子
        'ext_whitelist' => array('png', 'jpg', 'jpeg', 'gif'),
    ),

    /*
     * クーポン画像
     */
    'coupon' => array(
        // アップ画像一時保存先
        'path'          => APPPATH.'tmp/up_tmp/',
        // リネームする際のファイル名
        'tmp_suffix'    => 'tmp_coupon_%%%img_no%%%.jpg',
        // リネームする際のファイル名
        'suffix'        => 'coupon_%%%img_no%%%.jpg',
        // 保存する際のリネーム
        'auto_rename'   => true,
        // 許可する拡張子
        'ext_whitelist' => array('png', 'jpg', 'jpeg', 'gif'),
    ),

    /*
     * MENU: トップ画像
     */
    'menu_top' => array(
        // アップ画像一時保存先
        'path'          => APPPATH.'tmp/up_tmp/',
        // リネームする際のファイル名
        'suffix'        => 'menu_top_',
        // 保存する際のリネーム
        'auto_rename'   => true,
        // 許可する拡張子
        'ext_whitelist' => array('png', 'jpg', 'jpeg', 'gif'),
    ),

    /*
     * MENU: カテゴリー画像
     */
    'menu_category' => array(
        // アップ画像一時保存先
        'path'          => APPPATH.'tmp/up_tmp/',
        // リネームする際のファイル名
        'tmp_suffix'    => 'tmp_menu_cate_%%%img_no%%%.jpg',
        // リネームする際のファイル名
        'suffix'        => 'menu_cate_%%%img_no%%%.jpg',
        // 保存する際のリネーム
        'auto_rename'   => true,
        // 許可する拡張子
        'ext_whitelist' => array('png', 'jpg', 'jpeg', 'gif'),
    ),

    /*
     * MENU: 商品画像
     */
    'menu_item' => array(
        // アップ画像一時保存先
        'path'          => APPPATH.'tmp/up_tmp/',
        // リネームする際のファイル名
        'tmp_suffix'    => 'tmp_menu_item_%%%img_no%%%.jpg',
        // リネームする際のファイル名
        'suffix'        => 'menu_item_%%%img_no%%%.jpg',
        // 保存する際のリネーム
        'auto_rename'   => true,
        // 許可する拡張子
        'ext_whitelist' => array('png', 'jpg', 'jpeg', 'gif'),
    ),

    /*
     * NEWS
     */
    'news' => array(
        // アップ画像一時保存先
        'path'          => APPPATH.'tmp/up_tmp/',
        // リネームする際のファイル名
        'tmp_suffix'    => 'tmp_news_%%%img_no%%%.jpg',
        // リネームする際のファイル名
        'suffix'        => 'news_%%%img_no%%%.jpg',
        // 保存する際のリネーム
        'auto_rename'   => true,
        // 許可する拡張子
        'ext_whitelist' => array('png', 'jpg', 'jpeg', 'gif'),
    ),

);

