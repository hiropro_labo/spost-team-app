{include "common/header.tpl"}

<div id="content"><!-- 固定ヘッダー+余白分 -->

    <img src="/assets/img/app/{$TEAM->id()}/common/header_schedule.png" style="position:fixed; width:100%">

<div id="sche_main" style="padding-top:27%; width:100%">


    <div class="schedule_detail">
        <p>{$schedule->opened_at|date_format:"%Y年%m月%d日(%a)"|replace:"Sun":"日"|replace:"Mon":"月"|replace:"Tue":"火"|replace:"Wed":"水"|replace:"Thu":"木"|replace:"Fri":"金"|replace:"Sat":"土"}</p>
    </div>

    <div class="schedule_vs">
        <img src="/assets/img/app/{$TEAM->id()}/schedule/team{$schedule->enemy}.png" alt="" class="vs" />
    </div>

<div class="clear"></div>

<ul class="sche_s">
<li><span>会 場</span><span><img src="/assets/img/app/{$TEAM->id()}/schedule/arrow_icon.png" alt=">>" class="sche_green_arrow" /></span><span>{$schedule->place}</span></li>
<li><span>時 間</span><span><img src="/assets/img/app/{$TEAM->id()}/schedule/arrow_icon.png" alt=">>" class="sche_green_arrow" /></span><span>{$schedule->opened_at|date_format:"%H:%M"}</span></li>
<li><span>レコード戦</span><span><img src="/assets/img/app/{$TEAM->id()}/schedule/arrow_icon.png" alt=">>" class="sche_green_arrow" /></span><span>{$schedule->l_code}</span></li>
</ul>


<!-- 地図 -->
<div class="iframeMap">
{*<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3257.559986807861!2d139.66403449999996!3d35.26720099999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1z5qiq6aCI6LOA44Ki44Oq44O844OK!5e0!3m2!1sja!2sjp!4v1400747551300" frameborder="0" style="border:0"></iframe>*}
<iframe width="740" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.jp/?q={$schedule->place}&hl=ja&amp;ie=UTF8&amp;z=16&amp;brcurrent=3,0x34674e0fd77f192f:0xf54275d47c665244,1&amp;output=embed"></iframe>
</div>


</div>

{include "common/footer.tpl"}