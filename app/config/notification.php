<?php
/**
 * 設定ファイル：PUSH通知
 */

return array(
    /**
     * pemファイル保存先ディレクトリ
     */
    'pem_save_dir' => '/home/ipost/admin/app/pem/client/',

    /**
     * Entrust Root Certification Authority　保存パス
     */
    'entrust_root_cert_pem' => '/home/ipost/admin/app/pem/root/entrust_root_certification_authority.pem',

    /**
     * GCM Send URL
     */
    'gcm_send_url' => 'https://android.googleapis.com/gcm/send',

    /**
     * GCM API Key
     */
    'gcm_api_key' => 'AIzaSyC9X9KJyxTVepx7DXOFmD_pn4VN7UGOwH4',
);
