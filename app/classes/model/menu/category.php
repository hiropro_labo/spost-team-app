<?php
/**
 * Model Menu: 商品情報
 *
 * @author     Kouji Itahana
 */

class Model_Menu_Category extends Model_Crud_Shard_Image
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'MENU_CATEGORY';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * @var string 設定ファイルキー値
     */
    protected static $_conf_key = 'menu_category';

    /**
     * @var string
     */
    protected static $_user_id_column = 'client_id';

    /**
     * 表示順にソートされたリストを取得
     *
     * @param string $client_id
     * @return Array|NULL
     */
    public static function get_category_list_all($client_id)
    {
        $list = self::find_by(array(static::$_user_id_column => $client_id));
        if ( ! is_null($list))
        {
            usort($list, function($a, $b){
                return $a['position'] > $b['position'];
            });
        }

        return $list;
    }

    /**
     * 表示順にソートされたリストを取得（※表示設定のもののみ）
     *
     * @param string $client_id
     * @return boolean|unknown
     */
    public static function get_category_list($client_id)
    {
        $list = self::find_by(array(
            static::$_user_id_column => $client_id,
            'enable'    => 1
        ));

        if ( ! is_null($list))
        {
            usort($list, function($a, $b){
                return $a['position'] > $b['position'];
            });
        }

        return $list;
    }

    /**
     * Select用Array生成
     *
     * @return array
     */
    public static function list_opt_array($client_id)
    {
        $opt = array('' => '----- カテゴリー選択 -----');
        $list = self::find_by(array('client_id' => $client_id));
        foreach ($list as $entry)
        {
            $opt[$entry->id] = $entry->title;
        }
        return $opt;
    }

}