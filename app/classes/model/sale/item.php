<?php
use Fuel\Core\Model_Crud;

/**
 * Model 販売商品マスター
 *
 * @author     Kouji Itahana
 */

class Model_Master_Sale_Item extends Model_Crud
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'M_SALE_ITEM';

    /**
     * 商品タイプ名称取得
     *
     * @return string
     */
    public function type_name()
    {
        if (property_exists(static::$this, 'type'))
        {
            $result = Model_Master_Sale_Item_Type::find_by_pk($this->type);
            if ( ! is_null($result))
            {
                return $result->name;
            }
        }
        return "";
    }
}