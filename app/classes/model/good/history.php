<?php
/**
 * Model いいね履歴
 *
 * @author K.ITAHANA
 */

class Model_Good_History extends Model_Crud_Shard
{
    /**
     * @var  string  $_table_name  The table name
     */
    protected static $_table_name = 'GOOD_HISTORY';

    /**
     * @var  string  fieldname of created_at field, uncomment to use.
     */
    protected static $_created_at = 'created_at';

    /**
     * @var  string  fieldname of updated_at field, uncomment to use.
     */
    protected static $_updated_at = 'updated_at';

    /**
     * @var array datetime columns
     */
    protected static $_datetime_columns = array(
        'updated_at', 'created_at'
    );

    /**
     * @var string
     */
    protected static $_user_id_column = 'client_id';
}