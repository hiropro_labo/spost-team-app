<?php
/**
 * Result
 *
 * @package  app
 * @extends  Basecontroller
 */
class Controller_Result_Index extends Basecontroller
{
    const VIEW_FILE_PREFIX = 'result/';
    /**
     * action 前処理
     */
    protected function before_controller()
    {
        //static::$_client_id = $this->setup_shop();
    }

    /**
	 * TOP
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
	    $view = View::forge(self::VIEW_FILE_PREFIX.'index.tpl');

	    $image = self::$_team->contents()->result_top()->model();

	    $list  = self::$_team->contents()->schedule()->model_list();
	    foreach ($list as $rec)
	    {
	        $model_result      = self::$_team->contents()->result()->model_by_schedule_id($rec->id);
	        $rec->region_own   = self::$_team->contents()->schedule()->region_name($rec->client_id);
	        $rec->region_enemy = self::$_team->contents()->schedule()->region_name($rec->enemy);
	        $rec->score_own    = $model_result->score_own;
	        $rec->score_enemy  = $model_result->score_enemy;
	        $rec->judge        = $model_result->judge;
	    }

	    $view->set('image', $image);
	    $view->set('list',  $list);
	    return $view;
	}
}
