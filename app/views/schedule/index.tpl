{include "common/header.tpl"}

<div id="content"><!-- 固定ヘッダー+余白分 -->

  <img src="/assets/img/app/{$TEAM->id()}/common/header_schedule.png" style="position:fixed; width:100%">
  <img src="http://timg.spost.jp/app/schedule/top/{$TEAM->id()}/{$image->file_name}" style="position:fixed; width:100%">

  <div id="sche_main" style="padding-top:27%; width:100%">


{foreach from=$list item=item}
    <div class="schedule">
        {if $item->home_and_away == 0}
        <img src="/assets/img/app/{$TEAM->id()}/schedule/icon_away.png" alt="away" />
        {else}
        <img src="/assets/img/app/{$TEAM->id()}/schedule/icon_away.png" alt="away" />
        {/if}
        <p>{$item->opened_at|date_format:"%Y年%m月%d日(%a)"|replace:"Sun":"日"|replace:"Mon":"月"|replace:"Tue":"火"|replace:"Wed":"水"|replace:"Thu":"木"|replace:"Fri":"金"|replace:"Sat":"土"}</p>
    </div>

    <div class="schedule_vs">
        <a href="/schedule/detail/{$TEAM->id()}/{$item->id}"><img src="/assets/img/app/{$TEAM->id()}/schedule/team{$item->enemy}.png" alt="" class="vs" /><img src="/assets/img/app/{$TEAM->id()}/schedule/arrow.png" alt=">" class="vs_arrow" /></a>
    </div>

    <div class="clear"></div>
{/foreach}

  </div>
</div>

{include "common/footer.tpl"}